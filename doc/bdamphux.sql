﻿-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  ven. 08 déc. 2017 à 01:49
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bdamphux`
--

-- --------------------------------------------------------

--
-- Structure de la table `path`
--

CREATE TABLE `path` (
  `id_path` int(20) NOT NULL,
  `departure` varchar(255) NOT NULL,
  `arrival` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `user_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `report`
--

CREATE TABLE `report` (
  `id_report` int(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `place` varchar(255) NOT NULL,
  `report_at` datetime NOT NULL,
  `plus` int(20) NOT NULL,
  `minus` int(20) NOT NULL,
  `user_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `user_id` int(20) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `acces` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`user_id`, `mail`, `password`, `acces`) VALUES
(1, 'flo.97@free.fr', '$2y$12$PIbividqwABjqhpgVwtnf.MIrfoXqsT0H4ydMnbaoVhsmsqtk1QES', 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `path`
--
ALTER TABLE `path`
  ADD PRIMARY KEY (`id_path`),
  ADD KEY `contrainte path` (`user_id`);

--
-- Index pour la table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id_report`),
  ADD KEY `contraintes report` (`user_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `path`
--
ALTER TABLE `path`
  MODIFY `id_path` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `report`
--
ALTER TABLE `report`
  MODIFY `id_report` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `path`
--
ALTER TABLE `path`
  ADD CONSTRAINT `contrainte path` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `contraintes report` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

CREATE TABLE `Slide` (
  `num` INT(6) PRIMARY KEY,
  `question` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `morale` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
);

CREATE TABLE `Reponse` (
   `numrep` INT(6) PRIMARY KEY,
   `numquestion` INT(6) NOT NULL,
   `reponse` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
   `correcte` boolean NOT NULL default 0,
   `morale` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
);

ALTER TABLE `Reponse`
  ADD CONSTRAINT `frkey_num_reponse_slide` FOREIGN KEY (`numquestion`) REFERENCES `Slide` (`num`) ON DELETE CASCADE ON UPDATE CASCADE;
  
INSERT INTO Slide VALUES (1,"Hé petit ! Fais attention ! Tu es tout seul ? Oui monsieur, j’ai 10 ans quand même !","On peut déjà faire du vélo seul à partir de 10 ans mais il est conseillé d’attendre ses 12 ans.");
INSERT INTO Reponse VALUES (1,1,"8 ans",0,"Hé non!");
INSERT INTO Reponse VALUES (2,1,"10 ans",1,"Tu as raison!");
INSERT INTO Reponse VALUES (3,1,"12 ans",1,"Tu as raison!");
INSERT INTO Reponse VALUES (4,1,"14 ans",0,"Hé non!");

INSERT INTO Slide VALUES (2,"À cet âge, on est plus sûr de soi et on connait déjà mieux les dangers de la route. Tu veux en savoir plus ?","");
INSERT INTO Reponse VALUES (5,2,"Oui !",1,"Tout d’abord, le port du casque est obligatoire pour les moins de 12 ans. Cela permet de te protéger la tête mais aussi la nuque qui est très importante. Il faut aussi penser à tout ce qui t’entoure et donc d’être visible pour que les autres fassent attention à toi aussi ! Tu dois donc avoir des lumières sur ton vélo et une sonnette pour avertir les autres personnes ! ");
INSERT INTO Reponse VALUES (6,2,"Pas vraiment...",0,"Comme tu voudras. ");

INSERT INTO Slide VALUES (3,"La voiture qui venait allait très vite ! Si seulement il y avait un radar dans cette rue… Tu sais ce que c’est un radar ?","Le radar est une machine postée près de la route qui permet de connaître la vitesse des véhicules qui passent.");
INSERT INTO Reponse VALUES (7,3,"C’est une boîte qui permet de connaître la vitesse des gens !",1,"Exactement !");
INSERT INTO Reponse VALUES (8,3,"Euh… Ce n’est pas dans un sous-marin ?",0,"Ahah ! Pas vraiment non !");
INSERT INTO Reponse VALUES (9,3,"Un petit crustacé ?",0,"Ahah ! Pas vraiment non !");

INSERT INTO Slide VALUES (4,"S’ils vont trop vite, ils sont punis !","");
INSERT INTO Reponse VALUES (10,4,"Woa ! Ça a l’air super utile !",1,"Et pas qu’un peu ! Grâce à cette machine, les personnes qui conduisent trop vite font plus attention au moins à certains endroits et s’ils ne le font pas, ils doivent payer ! ");
INSERT INTO Reponse VALUES (11,4,"D’accord, mais du coup ce n’était pas totalement ma faute, si ?",0,"-	Tu as raison ! Celui ou celle qui a failli te renverser n’a pas fait attention autour de lui.");

INSERT INTO Slide VALUES (5,"-	Ah au fait ! Tu sais à quoi sert un rond-point ?","Un rond-point ou giratoire permet de remplacer une intersection afin de faciliter le déplacement des véhicules et les mener vers plusieurs destinations !");
INSERT INTO Reponse VALUES (12,5,"C’est un endroit sur la route pour mettre des décorations !",0,"Euh… ça c’est après…");
INSERT INTO Reponse VALUES (13,5,"C’est fait pour rendre plus facile le déplacement des véhicules !",1,"Tu résumes bien la chose !");
INSERT INTO Reponse VALUES (14,5,"Je n’en ai aucune idée !",0,"");
 
INSERT INTO Slide VALUES (6,"Et à ton avis, est-ce qu’on doit conduire différemment en fonction de la météo ?","On doit ralentir d’au moins 10 km/h en fonction de la limitation actuelle de là où l’on conduit ! Car il y a des nombreux dangers qui peuvent entraîner des accidents ! ");
INSERT INTO Reponse VALUES (15,6,"Oui ! On doit ralentir s’il ne fait pas beau !",1,"Exactement");
INSERT INTO Reponse VALUES (16,6,"o	Nan ! Ça ne change rien !",0,"Hé non!");

INSERT INTO Slide VALUES (7,"D’ailleurs en parlant d’accident j’en profite pour te dire de faire très attention à tout ce que tu consomme ! Je sais que t’es jeune mais la drogue, l’alcool et le téléphone provoquent beaucoup d’accidents !", "Aller je dois y aller. Au revoir petit.");
INSERT INTO Reponse VALUES (17,7,"D’accord ! Je ferais attention ! Merci beaucoup !",1,"");
INSERT INTO Reponse VALUES (18,7,"La drogue et l’alcool? ",0,"Oui ! Tous deux font que tu es beaucoup plus lent  à réagir ce qui peut provoquer des accidents ! ");
INSERT INTO Reponse VALUES (19,7,"o	En quoi le téléphone est dangereux ? ",0,"Si tu es sur ton téléphone au volant, tu n’es plus concentré sur la route et donc tu ne réagis plus à ce qu’il se passe autour de toi !");


