# Amphux
Nuit de l'Info - Décembre 2017

# Configuration du système
1) .htaccess 
-> à configurer si besoin
-> se trouve dans le dossier doc
-> à placer à la racine du projet

2) sql.sql
-> configuration de la base de donnée
-> se trouve dans le dossier doc

3) conf.ini (pour la bdd)
-> à configurer
-> se trouve dans le dossier doc
-> à placer dans src/conf/

4) configurez xamp - lamp ... 

5) composer
-> à la racine, effectuez la commande "composer install"

6) Plus qu'à lancer le site ! 
