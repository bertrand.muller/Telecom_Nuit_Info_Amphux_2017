<?php

require 'vendor/autoload.php';

use \Slim\Slim;
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$db -> addConnection(parse_ini_file("src/conf/conf.ini"));
$db -> setAsGlobal();
$db ->bootEloquent();

$app = new Slim();

$app->get('/', function () {
  (new \chatbot\ctrl\controller)->showHome();
});

$app->get('/slide/:id', function ($id){
    (new \chatbox\ctrl\controller)->showSlide($id);
})->name('slide');

$app->get('/fin', function (){
    (new \chatbox\ctrl\controller)->showEnd();
});

$app->get('/credit', function (){
    (new \chatbox\ctrl\controller)->showCredit();
});


$app->run();
