<?php

	/**
	 * PHP FsdlRequest Parser
	 *
	 * @version		Alpha 0.3
	 *
	 * @copyright	1999-2017 OP3FT
	 *
	 * This file is provided by the OP3FT to help you create your own
	 * dynamic Frogans sites.
	 *
	 * You are free to use, copy, distribute, modify, adapt, or share
	 * this file under the terms set forth in the
	 * COPYRIGHT-AND-LICENSE.txt file in this archive.
	 *
	 * For information about the PHP FsdlRequest Parser, see the
	 * README.txt file in this archive.
	 *
	 * @file	 	 FsdlRequestParser.php
	 */


	/**
	 * Undefined value.
	 */
	 define("FSDL_REQUEST_UNDEFINED",-1);

	/**
	 * Text representation for an undefined value.
	 */
	 define("FSDL_REQUEST_UNDEFINED_TOSTRING","[undefined]");


	/**
	 * FsdlRequestDocumentVersion
	 */
	class FsdlRequestDocumentVersion
	{
		/**
		 * Version 3.0
		 */
		const VERSION_3_0 = 3.0;

		/**
		 * Returns a text representation
		 *
		 * @param $value
		 * @return string
		 */
		public static function toString($value)
		{
			if ($value == FsdlRequestDocumentVersion::VERSION_3_0)
			{
				return "3.0";
			}
			else /* e.g. $value == FSDL_REQUEST_UNDEFINED */
			{
				return FSDL_REQUEST_UNDEFINED_TOSTRING;
			}
		}
	}


	/**
	 * FsdlRequestWanted
	 **/
	class FsdlRequestWanted
	{
		/**
		 * FSDL document
		 */
		const FSDL_DOCUMENT = 1;

		/**
		 * Auxiliary file
		 */
		const AUXILIARY_FILE = 2;

		/**
		 * Returns a text representation
		 *
		 * @param $value
		 * @return string
		 */
		public static function toString($value)
		{
			if ($value == FsdlRequestWanted::FSDL_DOCUMENT)
			{
				return "fsdl-document";
			}
			elseif ($value == FsdlRequestWanted::AUXILIARY_FILE)
			{
				return "auxiliary-file";
			}
			else /* e.g. $value == FSDL_REQUEST_UNDEFINED */
			{
				return FSDL_REQUEST_UNDEFINED_TOSTRING;
			}
		}
	}


	/**
	 * FsdlRequestNavigation
	 **/
	class FsdlRequestNavigation
	{
		/**
		 * Button
		 */
		const BUTTON = 1;

		/**
		 * Next
		 */
		const NEXT = 2;

		/**
		 * Redirect
		 */
		const REDIRECT = 3;

		/**
		 * Open
		 */
		const OPEN = 4;

		/**
		 * Returns a text representation
		 *
		 * @param $value
		 * @return string
		 */
		public static function toString($value)
		{
			if ($value == FsdlRequestNavigation::BUTTON)
			{
				return "button";
			}
			elseif ($value == FsdlRequestNavigation::NEXT)
			{
				return "next";
			}
			elseif ($value == FsdlRequestNavigation::REDIRECT)
			{
				return "redirect";
			}
			elseif ($value == FsdlRequestNavigation::OPEN)
			{
				return "open";
			}
			else /* e.g. $value == FSDL_REQUEST_UNDEFINED */
			{
				return FSDL_REQUEST_UNDEFINED_TOSTRING;
			}
		}
	}


	/**
	 * FsdlRequestKind
	 **/
	class FsdlRequestKind
	{
		/**
		 * Image
		 */
		const IMAGE = 1;

		/**
		 * Returns a text representation
		 *
		 * @param $value
		 * @return string
		 */
		public static function toString($value)
		{
			if ($value == FsdlRequestKind::IMAGE)
			{
				return "image";
			}
			else /* e.g. $value == FSDL_REQUEST_UNDEFINED */
			{
				return FSDL_REQUEST_UNDEFINED_TOSTRING;
			}
		}
	}


	/**
	 * FsdlRequest
	 *
	 * Class used to parse an FSDL-Request document sent by Frogans Player
	 * to the server hosting a Frogans site
	 **/
	class FsdlRequest
	{

		/**
		 * Maximum size of FSDL-Request documents, in bytes.
		 */
		const DOCUMENT_SIZE_MAX = 65536;


		/**
		 * Maximum number of session fields
		 */
		const SESSION_FIELDS_COUNT_MAX = 16;


		/**
		 * Maximum number of file fields
		 */
		const FILE_FIELDS_COUNT_MAX = 16;


		/**
		 * Maximum number of entry fields
		 */
		const ENTRY_FIELDS_COUNT_MAX = 1;


		/**
		* FsdlRequest constructor
		*/
		public function __construct()
		{
			$this->doClear();
		}


		/**
		* Clears an instance
		* @return void
		*/
		public function clear()
		{
			$this->doClear();
		}


		/**
		 * Initializes an instance
		 *
		 * @param $fsdlRequestDocumentVersion	Possible values are defined in FsdlRequestDocumentVersion
		 * @param string $receivedDocument 		FSDL-Request document received by the server;
		 * 										Document size, in bytes, must be less than or equal to DOCUMENT_SIZE_MAX;
		 * 										Encoding can be UTF-8 or UTF-16 (reflecting the encoding for the Frogans site)
		 * @param string $errorMessage	[out]	Error message returned if initialization fails
		 * @return bool
		 */
		public function initialize($fsdlRequestDocumentVersion, $receivedDocument, &$errorMessage)
		{
			return $this->doInitialize($fsdlRequestDocumentVersion, $receivedDocument, $errorMessage);
		}


		/**
		 * Gets fsdlRequestWanted
		 * Instance must be initialized.
		 *
		 * Always applicable.
		 *
		 * @return Value is one of the following constants:
		 * 		FsdlRequestWanted::FSDL_DOCUMENT
		 * 		FsdlRequestWanted::AUXILIARY_FILE
		 *		(FSDL_REQUEST_UNDEFINED if not initialized)
		 */
		public function getFsdlRequestWanted()
		{
			if ($this->initialized == FALSE)
			{
				return FSDL_REQUEST_UNDEFINED;
			}

			return $this->fsdlRequestWanted;
		}


		/**
		 * Gets fsdlRequestNavigation
		 * Instance must be initialized.
		 *
		 * Applicable only if fsdlRequestWanted is FsdlRequestWanted::FSDL_DOCUMENT.
		 *
		 * @return Value is one of the following constants:
		 * 		FsdlRequestNavigation::BUTTON
		 * 		FsdlRequestNavigation::NEXT
		 * 		FsdlRequestNavigation::REDIRECT
		 * 		FsdlRequestNavigation::OPEN
 		 *		(FSDL_REQUEST_UNDEFINED if not initialized or not applicable)
		 */
		public function getFsdlRequestNavigation()
		{
			if ($this->initialized == FALSE)
			{
				return FSDL_REQUEST_UNDEFINED;
			}

			return $this->fsdlRequestNavigation;
		}


		/**
		 * Gets fsdlRequestKind
		 * Instance must be initialized.
		 *
		 * Applicable only if fsdlRequestWanted is FsdlRequestWanted::AUXILIARY_FILE.
		 *
		 * @return Value is one of the following constants:
		 * 		FsdlRequestKind::IMAGE
 		 *		(FSDL_REQUEST_UNDEFINED if not initialized or not applicable)
		 */
		public function getFsdlRequestKind()
		{
			if ($this->initialized == FALSE)
			{
				return FSDL_REQUEST_UNDEFINED;
			}

			return $this->fsdlRequestKind;
		}


		/**
		 * Gets sessionFields
		 * Instance must be initialized.
		 *
		 * Always applicable.
		 *
		 * @return Value is an array of up to FsdlRequest::SESSION_FIELDS_COUNT_MAX key-value pairs
		 * 		   The array can be empty.
		 */
		public function getSessionFields()
		{
			if ($this->initialized == FALSE)
			{
				return array();
			}

			return $this->sessionFields;
		}


		/**
		 * Gets fileFields
		 * Instance must be initialized.
		 *
		 * Applicable only if fsdlRequestWanted is FsdlRequestWanted::FSDL_DOCUMENT
		 * 						and fsdlRequestNavigation is not FsdlRequestNavigation::OPEN
		 * 				   or
		 * 				   if fsdlRequestWanted is FsdlRequestWanted::AUXILIARY_FILE
		 *
		 * @return Value is an array of up to FsdlRequest::FILE_FIELDS_COUNT_MAX key-value pairs
		 * 		   The array can be empty.
		 */
		public function getFileFields()
		{
			if ($this->initialized == FALSE)
			{
				return array();
			}

			return $this->fileFields;
		}


		/**
		 * Gets entryFields
		 * Instance must be initialized.
		 *
		 * Applicable only if fsdlRequestWanted is FsdlRequestWanted::FSDL_DOCUMENT
		 * 						and fsdlRequestNavigation is FsdlRequestNavigation::BUTTON
		 *
		 * @return Value is an array of up to FsdlRequest::ENTRY_FIELDS_COUNT_MAX key-value pairs
		 * 		   The array can be empty.
		 */
		public function getEntryFields()
		{
			if ($this->initialized == FALSE)
			{
				return array();
			}

			return $this->entryFields;
		}


		/* ========================================================== */
		/* ====== The properties and methods below are private ====== */
		/* ========================================================== */


		private $initialized;
		private $fsdlRequestWanted;
		private $fsdlRequestNavigation;
		private $fsdlRequestKind;
		private $sessionFields;
		private $fileFields;
		private $entryFields;

		private function doClear()
		{
			$this->initialized = FALSE;
			$this->fsdlRequestWanted = FSDL_REQUEST_UNDEFINED;
			$this->fsdlRequestNavigation = FSDL_REQUEST_UNDEFINED;
			$this->fsdlRequestKind = FSDL_REQUEST_UNDEFINED;
			$this->sessionFields = array();
			$this->fileFields = array();
			$this->entryFields = array();
		}

		private function appendLine(&$errorMessage, $line)
		{
			if (trim($line) != "" and $line != null)  // ignore empty lines or lines containing spaces only
			{
				$errorMessage .= "Error: ".$line;
				if (substr($line, -1) != "\n")  // add an EOL character if $line does not have one
				{
					$errorMessage .= "\n";
				}
			}
		}

		private function doInitialize($fsdlRequestDocumentVersion, $receivedDocument, &$errorMessage)
		{
			$this->doClear();

			$errorMessage = "";

			// Check parameters

			if ($fsdlRequestDocumentVersion != FsdlRequestDocumentVersion::VERSION_3_0)
			{
				$this->appendLine($errorMessage,"fsdlRequestDocumentVersion parameter value is not supported in this version of the FsdlRequest parser.");
				return FALSE;
			}

			if ($receivedDocument === FALSE)	// The '===' operator may be required for a reliable comparison, e.g. if the value was returned by file_get_contents() and provided to initialize() directly
			{
				$this->appendLine($errorMessage,"receivedDocument parameter value equals FALSE.");
				return FALSE;
			}

			// Check the FSDL-Request document size

			if (strlen($receivedDocument) > self::DOCUMENT_SIZE_MAX)
			{
				$this->appendLine($errorMessage,"The document size is greater than the limit of ".self::DOCUMENT_SIZE_MAX." bytes.");
				return FALSE;
			}

			if (trim($receivedDocument, " \t\n\r") == "")   // Trims XML white spaces
			{
				$this->appendLine($errorMessage,"The document is empty or contains white spaces only.");
				return FALSE;
			}

			// Parse the FSDL-Request document in XML

			$previousUseErrors = libxml_use_internal_errors(TRUE);  // Activates custom error management

			$root = simplexml_load_string($receivedDocument);  // Returns a SimpleXMLElement object or FALSE

			if ($root === FALSE)	// The '===' operator is required for a reliable comparison
			{
				$this->appendLine($errorMessage,"receivedDocument value is not XML compliant.");

				$errorArray = libxml_get_errors();  // Returns an array containing LibXMLError objects representing errors (or an empty array if there is no errors)

				libxml_clear_errors(); // Clears the libxml error buffer

				foreach($errorArray as $error)
				{
					$this->appendLine($errorMessage,"[libxml error] ".$error->message);
				}

				libxml_use_internal_errors($previousUseErrors);  // Restores previous error management

				return FALSE;
			}
			else
			{
				libxml_use_internal_errors($previousUseErrors);  // Restores previous error management
			}

			// Todo: Raise error if XML declaration is not included in the document (XML parser does not fail currently)

			// Todo: Review the general functioning of the XML parser with regards to XML 1.0 (Fifth Edition) recommendation

			// Validate the FSDL-Request document

			// Validate the root element (<frogans-fsdl-request>)

			if ($root->getName() != 'frogans-fsdl-request')
			{
				$this->appendLine($errorMessage,"The name of the root element must be <frogans-fsdl-request>.");
				return FALSE;
			}

			$rootAttributes = $root->attributes();

			if ($rootAttributes->count() != 1)
			{
				$this->appendLine($errorMessage,"The <frogans-fsdl-request> element must have exactly 1 attribute.");
				return FALSE;
			}

			$rootAttributeVersion = $rootAttributes[0];

			if ($rootAttributeVersion->getName() != 'version')
			{
				$this->appendLine($errorMessage,"The name of the attribute of the <frogans-fsdl-request> element must be \"version\".");
				return FALSE;
			}

			if ($rootAttributeVersion != strval(FsdlRequestDocumentVersion::VERSION_3_0))
			{
				$this->appendLine($errorMessage,"The value of the \"version\" attribute of the <frogans-fsdl-request> element must be '3.0'.");
				return FALSE;
			}

			$rootChildren = $root->children();

			if ($rootChildren->count() != 4)
			{
				$this->appendLine($errorMessage,"The <frogans-fsdl-request> element must contain exactly 4 child elements.");
				return FALSE;
			}

			$rootChild1 = $rootChildren[0];
			$rootChild2 = $rootChildren[1];
			$rootChild3 = $rootChildren[2];
			$rootChild4 = $rootChildren[3];

			// Validate the first child element (<request>)

			if ($rootChild1->getName() != 'request')
			{
				$this->appendLine($errorMessage,"The name of the first child element of the <frogans-fsdl-request> element must be \"request\".");
				return FALSE;
			}

			$rootChild1Attributes = $rootChild1->attributes();

			if ($rootChild1Attributes->count() != 2)
			{
				$this->appendLine($errorMessage,"The <request> element must have exactly 2 attributes.");
				return FALSE;
			}

			$rootChild1AttributeWanted = $rootChild1Attributes['wanted'];

			if (isset($rootChild1AttributeWanted) == FALSE)
			{
				$this->appendLine($errorMessage,"The <request> element must have a \"wanted\" attribute.");
				return FALSE;
			}

			if (($rootChild1AttributeWanted != 'fsdl-document') &&
				($rootChild1AttributeWanted != 'auxiliary-file'))
			{
				$this->appendLine($errorMessage,"The value of the \"wanted\" attribute of the <frogans-fsdl-request> element must be 'fsdl-document' or 'auxiliary-file'.");
				return FALSE;
			}

			if ($rootChild1AttributeWanted == 'fsdl-document')
			{
				$rootChild1AttributeNavigation = $rootChild1Attributes['navigation'];

				if (isset($rootChild1AttributeNavigation) == FALSE)
				{
					$this->appendLine($errorMessage,"The <request> element must have a \"navigation\" attribute, since the value of its \"wanted\" attribute is 'fsdl-document'.");
					return FALSE;
				}

				if (($rootChild1AttributeNavigation != 'button') &&
					($rootChild1AttributeNavigation != 'next') &&
					($rootChild1AttributeNavigation != 'redirect') &&
					($rootChild1AttributeNavigation != 'open'))
				{
					$this->appendLine($errorMessage,"The value of the \"navigation\" attribute of the <request> element must be 'button', 'next', 'redirect', or 'open'.");
					return FALSE;
				}
			}
			else  // $rootChild1AttributeWanted == 'auxiliary-file'
			{
				$rootChild1AttributeKind = $rootChild1Attributes['kind'];

				if (isset($rootChild1AttributeKind) == FALSE)
				{
					$this->appendLine($errorMessage,"The <request> element must have a \"kind\" attribute, since the value of its \"wanted\" attribute is 'auxiliary-file'.");
					return FALSE;
				}

				if ($rootChild1AttributeKind != 'image')
				{
					$this->appendLine($errorMessage,"The value of the \"kind\" attribute of the <request> element must be 'image'.");
					return FALSE;
				}
			}

			$rootChild1Children = $rootChild1->children();

			if ($rootChild1Children->count() != 0)
			{
				$this->appendLine($errorMessage,"The <request> element must not contain any child element.");
				return FALSE;
			}

			// Validate the second child element (<session-fields>)

			$canContainFields = TRUE;

			if ($this->validateFields($rootChild2,
									  "session-fields",
									  "second",
									  $canContainFields,
									  self::SESSION_FIELDS_COUNT_MAX,
									  $errorMessage) == FALSE)
			{
				return FALSE;
			}

			// Validate the third child element (<file-fields>)

			$canContainFields = FALSE;
			if ($rootChild1AttributeWanted == 'fsdl-document')
			{
				if ($rootChild1AttributeNavigation != 'open')
				{
					$canContainFields = TRUE;
				}
			}
			else // $rootChild1AttributeWanted == 'auxiliary-file'
			{
				$canContainFields = TRUE;
			}

			if ($this->validateFields($rootChild3,
									  "file-fields",
									  "third",
									  $canContainFields,
									  self::FILE_FIELDS_COUNT_MAX,
									  $errorMessage) == FALSE)
			{
				return FALSE;
			}

			// Validate the fourth child element (<entry-fields>)

			$canContainFields = FALSE;
			if ($rootChild1AttributeWanted == 'fsdl-document')
			{
				if ($rootChild1AttributeNavigation == 'button')
				{
					$canContainFields = TRUE;
				}
			}

			if ($this->validateFields($rootChild4,
									  "entry-fields",
									  "fourth",
									  $canContainFields,
									  self::ENTRY_FIELDS_COUNT_MAX,
									  $errorMessage) == FALSE)
			{
				return FALSE;
			}

			// Todo: Validate unicity of key attribute value within child elements, or across the FSDL-Request document (if confirmed by FSDL specification)

			// Validation is successful, so we can now fill in the private properties

			$this->initialized = TRUE;

			if ($rootChild1AttributeWanted == 'fsdl-document')
			{
				$this->fsdlRequestWanted = FsdlRequestWanted::FSDL_DOCUMENT;

				if ($rootChild1AttributeNavigation == 'button')
				{
					$this->fsdlRequestNavigation = FsdlRequestNavigation::BUTTON;
				}
				elseif ($rootChild1AttributeNavigation == 'next')
				{
					$this->fsdlRequestNavigation = FsdlRequestNavigation::NEXT;
				}
				elseif ($rootChild1AttributeNavigation == 'redirect')
				{
					$this->fsdlRequestNavigation = FsdlRequestNavigation::REDIRECT;
				}
				else  // $rootChild1AttributeNavigation == 'open'
				{
					$this->fsdlRequestNavigation = FsdlRequestNavigation::OPEN;
				}
			}
			else  // $rootChild1AttributeWanted == 'auxiliary-file'
			{
				$this->fsdlRequestWanted = FsdlRequestWanted::AUXILIARY_FILE;

				// $rootChild1AttributeKind == 'image'

				$this->fsdlRequestKind = FsdlRequestKind::IMAGE;
			}

			$rootChild2Children = $rootChild2->children();
			if ($rootChild2Children->count() > 0)
			{
				foreach($rootChild2Children as $field)
				{
					$fieldAttributes = $field->attributes();
					$this->sessionFields[strval($fieldAttributes['key'])] = strval($field);
				}
			}

			$rootChild3Children = $rootChild3->children();
			if ($rootChild3Children->count() > 0)
			{
				foreach($rootChild3Children as $field)
				{
					$fieldAttributes = $field->attributes();
					$this->fileFields[strval($fieldAttributes['key'])] = strval($field);
				}
			}

			$rootChild4Children = $rootChild4->children();
			if ($rootChild4Children->count() > 0)
			{
				foreach($rootChild4Children as $field)
				{
					$fieldAttributes = $field->attributes();
					$this->entryFields[strval($fieldAttributes['key'])] = strval($field);
				}
			}

			return TRUE;
		}

		private function validateFields(SimpleXMLElement $fields,
										$elementName,
										$elementOrder,
										$canContainFields,
										$fieldCountMax,
										&$errorMessage)
		{
			if ($fields->getName() != $elementName)
			{
				$this->appendLine($errorMessage,"The name of the ".$elementOrder." child element of the <frogans-fsdl-request> element must be \"".$elementName."\".");
				return FALSE;
			}

			$fieldsAttributes = $fields->attributes();

			if ($fieldsAttributes->count() != 0)
			{
				$this->appendLine($errorMessage,"The <".$elementName."> element must not have any attribute.");
				return FALSE;
			}

			$fieldsChildren = $fields->children();

			$fieldsChildrenCount = $fieldsChildren->count();

			if (($fieldsChildrenCount > 0) && ($canContainFields == FALSE))
			{
				$this->appendLine($errorMessage,"The <".$elementName."> element must not contain any child element given the value of the attributes of the <request> element.");
				return FALSE;
			}

			if ($fieldsChildrenCount > $fieldCountMax)
			{
				if ($fieldCountMax != 1)
				{
					$this->appendLine($errorMessage,"The <".$elementName."> element must not contain more than ".$fieldCountMax." child elements.");
					return FALSE;
				}
				else  // $fieldCountMax == 1
				{
					$this->appendLine($errorMessage,"The <".$elementName."> element must not contain more than ".$fieldCountMax." child element.");
					return FALSE;
				}
			}

			if ($fieldsChildrenCount > 0)
			{
				foreach($fieldsChildren as $field)
				{
					if ($field->getName() != 'field')
					{
						$this->appendLine($errorMessage,"The name of the child elements of the <".$elementName."> element must be \"field\".");
						return FALSE;
					}

					$fieldAttributes = $field->attributes();

					if ($fieldAttributes->count() != 1)
					{
						$this->appendLine($errorMessage,"The <field> elements of the <".$elementName."> element must have exactly 1 attribute.");
						return FALSE;
					}

					$fieldAttributeKey = $fieldAttributes[0];

					if ($fieldAttributeKey->getName() != 'key')
					{
						$this->appendLine($errorMessage,"The name of the attribute of the <field> elements of the <".$elementName."> element must be \"key\".");
						return FALSE;
					}

					// Todo: Validate syntax of key attribute value:

					/* The value is a case-sensitive string containing between 1 and 24
					   characters (inclusive). Each character in the string is in the range from 'A'
					   to 'Z' inclusive, in the range from 'a' to 'z' inclusive, in the range from '0' to
					   '9' inclusive), or is either the '_' character or the '-' character. The string
					   cannot contain the ' ' (space) character. */

					// Todo: Validate syntax of text characters contained in the field element

				}
			}

			return TRUE;
		}
	}
?>
