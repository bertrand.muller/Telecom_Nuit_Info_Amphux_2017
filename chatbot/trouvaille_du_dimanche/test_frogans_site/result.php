<?php

	/**
	 * Sample Frogans Site for the PHP FsdlRequest Parser
	 *
	 * @version		0.3
	 *
	 * @copyright	1999-2017 OP3FT
	 *
	 * This PHP file generates an FSDL document. It is part of a sample
	 * Frogans site provided by the OP3FT to help you create your
	 * own dynamic Frogans sites based on the PHP FsdlRequest parser.
	 *
	 * You are free to use, copy, distribute, modify, adapt, or share this
	 * sample Frogans site.
	 *
	 * @file		result.php
	 */


	// 1. Include FSDLRequest class

	include_once('./FsdlRequestParser.php');	// Except for testing, this file should be located outside the server's document root


	// 2. Check server request method

	$serverRequestMethod = $_SERVER['REQUEST_METHOD'];

	if ($serverRequestMethod != 'POST')	// This should never occur when navigating this sample Frogans site
	{
		exit(1);
	}


	// 3. Get content of HTTP body received by server

	$serverRequestBody = file_get_contents("php://input");


	// 4. Create and initialize $fsdlRequest

	$fsdlRequest = new FsdlRequest();

	$fsdlRequestDocumentVersion = FSDLRequestDocumentVersion::VERSION_3_0;
	$receivedDocument = $serverRequestBody;
	$errorMessage = "";

	$result = $fsdlRequest->initialize($fsdlRequestDocumentVersion, $receivedDocument, $errorMessage);

	if ($result == FALSE)	// This should never occur when navigating this sample Frogans site
	{
		exit(1);
	}


	// 5. Get all available data from $fsdlRequest

	$fsdlRequestWanted = $fsdlRequest->getFsdlRequestWanted();
					//	Value is always applicable
					//	Value is one of the following constants:
					//		FsdlRequestWanted::FSDL_DOCUMENT
					//		FsdlRequestWanted::AUXILIARY_FILE

	$fsdlRequestNavigation = $fsdlRequest->getFsdlRequestNavigation();
					//	Value is applicable only if $fsdlRequestWanted is FsdlRequestWanted::FSDL_DOCUMENT
					//	If not applicable, value is FSDL_REQUEST_UNDEFINED
					//	If applicable, value is one of the following constants:
					//		FsdlRequestNavigation::BUTTON
					//		FsdlRequestNavigation::NEXT
					//		FsdlRequestNavigation::REDIRECT
					//		FsdlRequestNavigation::OPEN

	$fsdlRequestKind = $fsdlRequest->getFsdlRequestKind();
					//	Value is applicable only if $fsdlRequestWanted is FsdlRequestWanted::AUXILIARY_FILE
					//	If not applicable, value is FSDL_REQUEST_UNDEFINED
					//	If applicable, possible values are the following constants:
					//		FsdlRequestKind::IMAGE

	$sessionFields = $fsdlRequest->getSessionFields();
					//	Value is always applicable
					//	Value is an array of up to FsdlRequest::SESSION_FIELDS_COUNT_MAX key-value pairs.
					//	The array can be empty.

	$fileFields = $fsdlRequest->getFileFields();
					//	Value is applicable only if $fsdlRequestWanted is FsdlRequestWanted::FSDL_DOCUMENT
					//									and $fsdlRequestNavigation is not FsdlRequestNavigation::OPEN
					//						  or if $fsdlRequestWanted is FsdlRequestWanted::AUXILIARY_FILE
					//	If not applicable, value is an empty array.
					//	If applicable, value is an array of up to FsdlRequest::FILE_FIELDS_COUNT_MAX key-value pairs.
					//	The array can be empty.

	$entryFields = $fsdlRequest->getEntryFields();
					//	Value is applicable only if $fsdlRequestWanted is FsdlRequestWanted::FSDL_DOCUMENT
					//									and $fsdlRequestNavigation is FsdlRequestNavigation::BUTTON
					//	If not applicable, value is an empty array.
					//	If applicable, value is an array of up to FsdlRequest::ENTRY_FIELDS_COUNT_MAX key-value pairs.
					//	The array can be empty.


	// 6. Check request context

	if ($fsdlRequestWanted != FsdlRequestWanted::FSDL_DOCUMENT)	// This should never occur when navigating this sample Frogans site
	{
		exit(1);
	}

	if ($fsdlRequestNavigation != FsdlRequestNavigation::BUTTON)	// This should never occur when navigating this sample Frogans site
	{
		exit(1);
	}


	// 7. Prepare content to be displayed on this result Frogans slide

	$fsdlRequestWantedText = FsdlRequestWanted::toString($fsdlRequestWanted);
	$fsdlRequestNavigationText = FsdlRequestNavigation::toString($fsdlRequestNavigation);

	$sessionFieldsCount = count ($sessionFields);
	$fileFieldsCount = count ($fileFields);
	$entryFieldsCount = count ($entryFields);

	if (($sessionFieldsCount + $fileFieldsCount + $entryFieldsCount) > 7)	// This should never occur when navigating this sample Frogans site
	{
		exit(1);
	}

	function EscapeFieldValue($value)		// Escape "&", "<" and " " characters in a field value to be displayed
	{
		$tmp = str_replace("&", "&amp;", $value);
		$tmp = str_replace("<", "&lt;", $tmp);
		$tmp = str_replace(" ", "&#xA0;", $tmp);
		return $tmp;
	}

	echo "<?xml version='1.0' encoding='utf-8'?>\n";	// PHP generates a syntax error with an Apache server if XML declaration is placed directly before the <frogans-fsdl> element below

	// See additional PHP instructions embedded in the FSDL elements below

?>

<frogans-fsdl version='3.0'>

	<!-- This FSDL document was generated using PHP. It is part of a sample
		 Frogans site provided by the OP3FT to help you create your
		 own dynamic Frogans sites based on the PHP FsdlRequest parser.

		 You are free to use, copy, distribute, modify, adapt, or share this
		 sample Frogans site. -->


	<!-- Result slide -->


	<!-- Background -->

	<resdraw resid='background' size='640,480' figure='rect' stroke='off' color='#202020'/>

	<layer layerid='layer1' leapout='all' resref='background' align='left-top' pos='0,0' combine='add'/>

	<resdraw resid='edging' size='620,460' figure='rect' stroke='on' thick='10'/>

	<layer layerid='layer2' leapout='all' resref='edging' align='left-top' pos='10,10' combine='cutout'/>


	<!-- Home button -->

	<file fileid='home_slide' nature='static' name='/home.fsdl'/>

	<respath resid='home_not_selected' size='40,40' crop='auto' stroke='off' spread='on' color='#00C0ff'>
		Ju:0,50; Li:100,0; Li:100,100;
	</respath>

	<respath resid='home_selected' size='40,40' crop='auto' stroke='off' spread='on' color='#40ffff'>
		Ju:0,50; Li:100,0; Li:100,100;
	</respath>

	<button buttonid='button1' goto='slide' fileref='home_slide'>

		<layer layerid='layer3' leapout='lead' resref='home_not_selected' align='left-top' pos='560,35' combine='clip' visible='not-selected'/>

		<layer layerid='layer4' leapout='lead' resref='home_selected' align='left-top' pos='560,35' combine='clip' visible='selected'/>

	</button>


	<!-- Information -->

	<setfont fontid='title'>
		<font scripts='default' pfont='110-1-mono-r' height='16' color='#ff8000'/>
	</setfont>

	<setfont fontid='white'>
		<font scripts='default' pfont='110-1-mono-r' height='9' color='#ffffff'/>
	</setfont>

	<setfont fontid='orange'>
		<font scripts='default' pfont='110-1-mono-r' height='9' color='#ff8000'/>
	</setfont>

	<setfont fontid='yellow'>
		<font scripts='default' pfont='110-1-mono-r' height='9' color='#ffff00'/>
	</setfont>

	<restext resid='info' size='560,420' orientation='h-ttb-ltr' fontref='white' talign='begin'>

		<text fontref='title'>Generated result</text>

		<text>An FSDL-Request document was received by the server with this data:</text>

		<text></text>

		<text fontref='orange'>Wanted: <?php  echo $fsdlRequestWantedText;  ?></text>

		<text fontref='orange'>Navigation: <?php  echo $fsdlRequestNavigationText;  ?></text>

		<text fontref='orange'>Session fields count: <?php  echo $sessionFieldsCount;  ?></text>

		<?php	if ($sessionFieldsCount)  {  foreach($sessionFields as $key => $value)  {  $insert= "&#xA0;&#xA0;".$key." => ".EscapeFieldValue ($value); ?>

		<text fontref='yellow'><?php  echo $insert;  ?></text>

		<?php	}  }  ?>

		<text fontref='orange'>File fields count: <?php  echo $fileFieldsCount;  ?></text>

		<?php	if ($fileFieldsCount)  {  foreach($fileFields as $key => $value)  {  $insert= "&#xA0;&#xA0;".$key." => ".EscapeFieldValue ($value); ?>

		<text fontref='yellow'><?php  echo $insert;  ?></text>

		<?php	}  }  ?>

		<text fontref='orange'>Entry fields count: <?php  echo $entryFieldsCount;  ?></text>

		<?php	if ($entryFieldsCount)  {  foreach($entryFields as $key => $value)  {  $insert= "&#xA0;&#xA0;".$key." => ".EscapeFieldValue ($value); ?>

		<text fontref='yellow'><?php  echo $insert;  ?></text>

		<?php	}  }  ?>

	</restext>

	<layer layerid='layer5' leapout='all' resref='info' align='left-top' pos='40,40'  combine='add'/>

</frogans-fsdl>
