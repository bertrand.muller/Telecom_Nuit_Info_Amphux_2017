CREATE TABLE `Slide` (
  `num` INT(6) PRIMARY KEY,
  `question` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
);

CREATE TABLE `Reponse` (
   `numrep` INT(6) PRIMARY KEY,
   `numquestion` INT(6) NOT NULL,
   `reponse` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
   `correcte` boolean NOT NULL default 0
);

ALTER TABLE `Reponse`
  ADD CONSTRAINT `contrainte path` FOREIGN KEY (`numquestion`) REFERENCES `Slide` (`num`) ON DELETE CASCADE ON UPDATE CASCADE;
