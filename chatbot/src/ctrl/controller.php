<?php
namespace chatbot;

public controller{

    public static function showSlide($id){
      $slide=Slide::where('num', '=, $id')->first();
      $vue=new ../view/Vue($slide, "Slide");
      echo $vue->render();
    }

    public static function showCredit(){
      $vue=new ../view/Vue(null, "Credit");
      echo $vue->render();
    }

    public static function showHome(){
      $vue=new ../view/Vue(null, "Home");
      echo $vue->render();
    }

    public static function showEnd(){
      $vue=new ../view/Vue(null, "End");
      echo $vue->render();
    }
}
