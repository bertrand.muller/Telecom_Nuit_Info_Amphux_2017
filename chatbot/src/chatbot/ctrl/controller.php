<?php
namespace chatbot\ctrl;

class controller{

    public static function showSlide($id){
      $slide=Slide::where('num', '=, $id')->first();
      $vue=new \chatbot\view\Vue($slide, "Slide");
      echo $vue->render();
    }

    public static function showCredit(){
      $vue=new \chatbot\view\Vue(null, "Credit");
      echo $vue->render();
    }

    public static function showHome(){
      $vue=new \chatbot\view\Vue(null, "Home");
      echo $vue->render();
    }

    public static function showEnd(){
      $vue=new \chatbot\view\Vue(null, "End");
      echo $vue->render();
    }
}
