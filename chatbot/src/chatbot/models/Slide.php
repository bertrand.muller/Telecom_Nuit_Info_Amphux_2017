<?php

namespace chatbot\models;

public class Slide extends \Illuminate\Database\Eloquent\Model{

  protected $table='slide';
  protected $primaryKey='id';
  public $timestamps=false;
}
