<?php

namespace chatbot\models;

public class Reponse extends \Illuminate\Database\Eloquent\Model{

  protected $table='reponse';
  protected $primaryKey='id';
  public $timestamps=false;
}
