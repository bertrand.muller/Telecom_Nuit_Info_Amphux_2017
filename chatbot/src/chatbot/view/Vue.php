<?php

namespace chatbot\view;

class Vue{

      public $slide;
      public $selecteur;

       public function __construct($sl,$selec){
       	      $this->slide=$sl;
	            $this->selecteur=$selec;
	     }

       public function render(){
        switch($this->selecteur){
       	      case "Home":

	      $content= <<<END
 <!-- fenetre principale-->
	<resdraw resid='resdraw0' size='640,480' figure='roundrect' stroke='off' round='30,30' color='#000000' />
	<layer layerid='canvas' leapout='all' resref='resdraw0' opacity='100' pos='0,0' align='left-top' combine='add' />


 	<setfont fontid='fontTitre'>

	  <font scripts='default' pfont='128-4-serif-r' height='20' color='#ffffff' />
	</setfont>

	<restext resid='titre' size='350,250' orientation='h-ttb-ltr' fontref='fontTitre' >
		<text>Le Titre wesh</text>
	</restext>

	<layer layerid='layer38' leapout='all' resref='titre' pos='200,100' align='left-top' combine='clip' />


	<!-- le font pour les boutons -->
	<resdraw resid='resdraw111' size='150,35' figure='roundrect' stroke='off' round='30,30' color='#000033' />

	<!-- ce qui entoure les reponses que l'utilisateur choisi -->
	<resdraw resid='resdraw222' size='150,35' figure='roundrect' stroke='on' thick='3' round='10,10' color='#ff0000' />
	<setfont fontid='setfont1'>
		<font scripts='default' pfont='128-4-serif-r' height='10' color='#ffffff' />
	</setfont>

	<restext resid='creds' size='200,30' orientation='h-ttb-ltr' fontref='setfont1' >
		<text>Crédits</text>
	</restext>

	<restext resid='deb' size='200,30' orientation='h-ttb-ltr' fontref='setfont1' >
	  <text>Commencer</text>
	</restext>

	<layer layerid='layer30' leapout='all' resref='creds' pos='115,332' align='left-top' combine='clip' />

	<layer layerid='layer31' leapout='all' resref='deb' pos='115,362' align='left-top' combine='clip' />

	<file fileid='address' nature='static' name='src/chatbot/credits.fsdl' />
	<file fileid='comm' nature='static' name='/commencer.fsdl' />

	<button buttonid='button2' goto='slide' fileref='comm' >
		<layer layerid='layer77' leapout='lead' resref='resdraw111' opacity='1' pos='105,330' align='left-top' combine='clip' visible='always' reactivity='#01'/>
		<layer layerid='layer88' leapout='lead' resref='resdraw222' opacity='100' pos='105,330' align='left-top' combine='clip' visible='selected'/>
	</button>

	<button buttonid='button1' goto='slide' fileref='address' >
		<layer layerid='layer777' leapout='lead' resref='resdraw111' opacity='1' pos='105,360' align='left-top' combine='clip' visible='always' reactivity='#01'/>
		<layer layerid='layer888' leapout='lead' resref='resdraw222' opacity='100' pos='105,360' align='left-top' combine='clip' visible='selected'/>
	</button>
END;
	      break;
      case "Slide":
	         $texte=$this->slide->getTexte();
	          $q1=$this->slide->getReponse(1);
	           $q2=$this->slide->getReponse(2);
	            $q3=$this->slide->getReponse(3);
	             $q4=$this->slide->getReponse(4);
	              $r1=$this->slide->getSuiv(1);
	               $r2=$this->slide->getSuiv(2);
	                $r3=$this->slide->getSuiv(3);
	                 $r4=$this->slide->getSuiv(4);
	                  $content = <<<END
	<setfont fontid='font1'>
		<font scripts='default' pfont='128-4-serif-r' height='15' color='#3366ff' />
	</setfont>

	<setfont fontid='font2'>
		<font scripts='default' pfont='128-4-serif-r' height='10' color='#ffffff' />
	</setfont>

	<!-- Images -->
	<file fileid='file0' nature='static' name='/images/sprite_1.png' cache='on' />
	<file fileid='file1' nature='static' name='/images/sprite_2.png' cache='on' />
	<resimage resid='resimage0' size='300,300' fileref='file0' />
	<resimage resid='resimage1' size='300,300' fileref='file1' />



	<resdraw resid='resdraw_TR' size='420,300' figure='roundrect' stroke='off' round='15,15' color='#000000' />

	<restext resid='txt_Question' size='420,250' orientation='h-ttb-ltr' fontref='font1'>
		<!--<text>Bla bla bla c'est une question</text>-->
		<text>$texte</text>
	</restext>


	<!-- Place Image and Red Box -->
	<layer layerid='layer1' leapout='all' resref='resimage0' pos='-50,10' align='left-top' combine='clip' />

	<layer layerid='layer_TR' leapout='all' resref='resdraw_TR' opacity='60' pos='640,2' align='right-top' combine='add' />
	<layer layerid='layer_TR2' leapout='all' resref='txt_Question' pos='240,30' align='left-top' combine='clip' />

	<!-- Botom part -->

	<resdraw resid='resdraw1' size='640,170' figure='roundrect' stroke='off' round='30,30' color='#ff9999' />
	<resdraw resid='resdraw2' size='640,170' figure='roundrect' stroke='on' thick='5' round='30,30' color='#ffffff' />
	<layer layerid='layer16' leapout='all' resref='resdraw1' opacity='80' pos='639,479' align='right-bottom' combine='add' />
	<layer layerid='layer17' leapout='all' resref='resdraw2' opacity='90' pos='639,479' align='right-bottom' combine='add' />

	<restext resid='restext2' size='560,30' orientation='h-ttb-ltr' fontref='font2'>
		<text>$q1</text>
	</restext>

	<restext resid='restext3' size='560,30' orientation='h-ttb-ltr' fontref='font2' >
		<text>$q2</text>
	</restext>

	<restext resid='restext4' size='560,30' orientation='h-ttb-ltr' fontref='font2'>
		<text>$q3</text>
	</restext>

	<restext resid='restext5' size='560,30' orientation='h-ttb-ltr' fontref='font2'>
		<text>$q4</text>
	</restext>

	<layer layerid='layer30' leapout='all' resref='restext2' pos='15,325' align='left-top' combine='clip' />
	<layer layerid='layer31' leapout='all' resref='restext3' pos='15,362' align='left-top' combine='clip' />
	<layer layerid='layer32' leapout='all' resref='restext4' pos='15,397' align='left-top' combine='clip' />
	<layer layerid='layer33' leapout='all' resref='restext5' pos='15,432' align='left-top' combine='clip' />

	<file fileid='name' nature='static' name='$r1' />
	<file fileid='address' nature='static' name='$r2' />
	<file fileid='something' nature='static' name='$r3' />
	<file fileid='other' nature='static' name='$r4' />

	<resdraw resid='resdraw111' size='600,35' figure='roundrect' stroke='off' round='30,30' color='#000033' />
	<resdraw resid='resdraw222' size='600,35' figure='roundrect' stroke='on' thick='3' round='10,10' color='#ffffff' />

	<button buttonid='button0' goto='slide' fileref='name' >
		<layer layerid='layer77' leapout='lead' resref='resdraw111' opacity='1' pos='10,325' align='left-top' combine='clip' visible='always' reactivity='#01'/>
		<layer layerid='layer88' leapout='lead' resref='resdraw222' opacity='100' pos='10,325' align='left-top' combine='clip' visible='selected'/>
		<layer layerid='layer_M1' leapout='lead' resref='resimage1' pos='-50,10' align='left-top' combine='clip' visible='selected'/>
	</button>

	<button buttonid='button1' goto='slide' fileref='address' >
		<layer layerid='layer777' leapout='lead' resref='resdraw111' opacity='1' pos='10,360' align='left-top' combine='clip' visible='always' reactivity='#01'/>
		<layer layerid='layer888' leapout='lead' resref='resdraw222' opacity='100' pos='10,360' align='left-top' combine='clip' visible='selected'/>
		<layer layerid='layer_M2' leapout='lead' resref='resimage1' pos='-50,10' align='left-top' combine='clip' visible='selected'/>
	</button>

	<button buttonid='button2' goto='slide' fileref='something' >
		<layer layerid='layer7777' leapout='lead' resref='resdraw111' opacity='1' pos='10,395' align='left-top' combine='clip' visible='always' reactivity='#01'/>
		<layer layerid='layer8888' leapout='lead' resref='resdraw222' opacity='100' pos='10,395' align='left-top' combine='clip' visible='selected'/>
		<layer layerid='layer_M3' leapout='lead' resref='resimage1' pos='-50,10' align='left-top' combine='clip' visible='selected'/>
	</button>

	<button buttonid='button3' goto='slide' fileref='other' >
		<layer layerid='layer77777' leapout='lead' resref='resdraw111' opacity='1' pos='10,430' align='left-top' combine='clip' visible='always' reactivity='#01'/>
		<layer layerid='layer88888' leapout='lead' resref='resdraw222' opacity='100' pos='10,430' align='left-top' combine='clip' visible='selected'/>
		<layer layerid='layer_M4' leapout='lead' resref='resimage1' pos='-50,10' align='left-top' combine='clip' visible='selected'/>
	</button>
END;
	         break;

	      case "End":
	      $content="
	      <!-- Font -->
	<setfont fontid='font1'>
		<font scripts='default' pfont='128-4-serif-r' height='15' color='#3366ff' />
	</setfont>

	<setfont fontid='font2'>
		<font scripts='default' pfont='128-4-serif-r' height='10' color='#ffffff' />
	</setfont>


	<!-- Images -->
	<file fileid='file0' nature='static' name='/images/sprite_1.png' cache='on' />
	<file fileid='file1' nature='static' name='/images/sprite_2.png' cache='on' />
	<resimage resid='resimage0' size='300,300' fileref='file0' />
	<resimage resid='resimage1' size='300,300' fileref='file1' />


	<!-- Black box -->
	<resdraw resid='resdraw_TR' size='420,420' figure='roundrect' stroke='off' round='15,15' color='#000000' />

	<restext resid='txt_Question' size='420,350' orientation='h-ttb-ltr' fontref='font1'>
		<text>Merci d'avoir suivi ce questionnaire de culture générale sur la sécurité routière !</text>
		<text>Tu peux recommencer si tu veux revoir tout ce qui t'a été présenté !</text>
	</restext>


	<!-- Place Image and Red Box -->
	<layer layerid='layer1' leapout='all' resref='resimage0' pos='-50,10' align='left-top' combine='clip' />
	<layer layerid='layer_TR' leapout='all' resref='resdraw_TR' opacity='60' pos='640,2' align='right-top' combine='add' />
	<layer layerid='layer_TR2' leapout='all' resref='txt_Question' pos='240,30' align='left-top' combine='clip' />




	<!-- Botom part -->

	<!-- Red box -->
	<resdraw resid='resdraw1' size='640,50' figure='roundrect' stroke='off' round='30,30' color='#ff9999' />

	<!-- White border -->
	<resdraw resid='resdraw2' size='640,50' figure='roundrect' stroke='on' thick='5' round='30,30' color='#ffffff' />
	<layer layerid='layer16' leapout='all' resref='resdraw1' opacity='80' pos='639,479' align='right-bottom' combine='add' />
	<layer layerid='layer17' leapout='all' resref='resdraw2' opacity='90' pos='639,479' align='right-bottom' combine='add' />

	<!-- Prepare a text resource for answer 3 -->
	<restext resid='restext5' size='560,30' orientation='h-ttb-ltr' fontref='font2'>
		<text>Revenir au début</text>
	</restext>
	<layer layerid='layer33' leapout='all' resref='restext5' pos='15,432' align='left-top' combine='clip' />

	<!-- Assemble buttons -->
	<file fileid='R1' nature='static' name='A_CHANGER' />

	<!-- Black  -->
	<resdraw resid='resdraw111' size='600,35' figure='roundrect' stroke='off' round='30,30' color='#000033' />

	<!-- White border -->
	<resdraw resid='resdraw222' size='600,35' figure='roundrect' stroke='on' thick='3' round='10,10' color='#ffffff' />

	<!-- buttons -->
	<button buttonid='button3' goto='slide' fileref='R1' >
		<layer layerid='layer77777' leapout='lead' resref='resdraw111' opacity='1' pos='10,430' align='left-top' combine='clip' visible='always' reactivity='#01'/>
		<layer layerid='layer88888' leapout='lead' resref='resdraw222' opacity='100' pos='10,430' align='left-top' combine='clip' visible='selected'/>
		<layer layerid='layer_M4' leapout='lead' resref='resimage1' pos='-50,10' align='left-top' combine='clip' visible='selected'/>
	</button>";
	         break;


	      case "Credit":
	      $content= "

    <!-- Font -->
	<setfont fontid='font1'>
		<font scripts='default' pfont='128-4-serif-r' height='15' color='#3366ff' />
	</setfont>
	<setfont fontid='font2'>
		<font scripts='default' pfont='128-4-serif-r' height='10' color='#ffffff' />
	</setfont>

	<!-- Top part -->

	<!-- Images -->
	<file fileid='file0' nature='static' name='/images/sprite_1.png' cache='on' />
	<file fileid='file1' nature='static' name='/images/sprite_2.png' cache='on' />
	<resimage resid='resimage0' size='300,300' fileref='file0' />
	<resimage resid='resimage1' size='300,300' fileref='file1' />

	<!-- Black box -->
	<resdraw resid='resdraw_TR' size='420,400' figure='roundrect' stroke='off' round='15,15' color='#000000' />

	<!-- Text -->
	<restext resid='txt_Question' size='420,400' orientation='h-ttb-ltr' fontref='font1'>
		<text>A Propos</text>
		<text></text>
		<text>Equipe Amphux</text>
		<text>Nancy</text>
		<text>Nuit de l'info</text>
		<text>07/12-08/12 2017</text>
	</restext>

	<!-- Place Image and Red Box -->
	<layer layerid='layer1' leapout='all' resref='resimage0' pos='-50,10' align='left-top' combine='clip' />
	<layer layerid='layer_TR' leapout='all' resref='resdraw_TR' opacity='60' pos='640,2' align='right-top' combine='add' />
	<layer layerid='layer_TR2' leapout='all' resref='txt_Question' pos='240,30' align='left-top' combine='clip' />





	<!-- Botom part -->

	<!-- Red box -->
	<resdraw resid='resdraw1' size='640,50' figure='roundrect' stroke='off' round='30,30' color='#ff9999' />

	<!-- White border -->
	<resdraw resid='resdraw2' size='640,50' figure='roundrect' stroke='on' thick='5' round='30,30' color='#ffffff' />
	<layer layerid='layer16' leapout='all' resref='resdraw1' opacity='80' pos='639,479' align='right-bottom' combine='add' />
	<layer layerid='layer17' leapout='all' resref='resdraw2' opacity='90' pos='639,479' align='right-bottom' combine='add' />

	<!-- Prepare a text resource for answer 3 -->
	<restext resid='restext5' size='560,30' orientation='h-ttb-ltr' fontref='font2'>
		<text>Retour vers l'acceuil</text>
	</restext>
	<layer layerid='layer33' leapout='all' resref='restext5' pos='15,435' align='left-top' combine='clip' />

	<!-- Assemble buttons -->
	<file fileid='R1' nature='static' name='A_CHANGER' />

	<!-- Red  -->
	<resdraw resid='resdraw111' size='600,35' figure='roundrect' stroke='off' round='30,30' color='#000033' />

	<!-- White border -->
	<resdraw resid='resdraw222' size='600,35' figure='roundrect' stroke='on' thick='3' round='10,10' color='#ffffff' />

	<!-- buttons -->
	<button buttonid='button3' goto='slide' fileref='other' >
		<layer layerid='layer77777' leapout='lead' resref='resdraw111' opacity='1' pos='10,435' align='left-top' combine='clip' visible='always' reactivity='#01'/>
		<layer layerid='layer88888' leapout='lead' resref='resdraw222' opacity='100' pos='10,435' align='left-top' combine='clip' visible='selected'/>
		<layer layerid='layer_M4' leapout='lead' resref='resimage1' pos='-50,10' align='left-top' combine='clip' visible='selected'/>
	</button>";
	         break;
	 }

$xml = <<<END
<?xml version='1.0' encoding='utf-8'?>

<frogans-fsdl version='3.0'>

$content

</frogans-fsdl>
END;
  return $xml;
}
}
