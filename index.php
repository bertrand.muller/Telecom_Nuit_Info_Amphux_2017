<?php
//pour utiliser les variables de session
session_start();

require 'vendor/autoload.php';

\conf\Eloquent::init('src/conf/conf.ini');

$app = new \Slim\Slim;
if(!isset($_SESSION['msg'])){
    $_SESSION['msg'] = array();
}

$app->get('/', function(){
    if(isset($_SESSION['mail'])){
        print "<div>Vous êtes connecté en tant que {$_SESSION['mail']}</div>";
        (new amphux\controleurs\ControleurUser())->index();
    }else {
        (new amphux\controleurs\ControleurUser())->index();
    }
})->name("accueil");

$app->get('/deconnexion/',function(){
    (new amphux\controleurs\ControleurUser())->deconnexion();
})->name("deconnexion");

$app->get('/connexion/',function(){
    print ((new amphux\vues\VueNavigation())->render(\amphux\vues\VueNavigation::AFF_CONNEXION));
});

$app->get('/inscription/',function(){
    print ((new amphux\vues\VueNavigation())->render(\amphux\vues\VueNavigation::AFF_INSCRIPTION));
});

$app->post('/connexion/', function(){
    (new amphux\controleurs\ControleurUser())->connexion();
})->name("connexion");

$app->post('/inscription/', function(){
    (new amphux\controleurs\ControleurUser())->inscription();
})->name("inscription");

/* TO DELETE */
$app->get('/bad/', function(){
    (new amphux\controleurs\Error404Controller())->affichageErreur();
})->name("Error");

$app->get('/soirees/', function(){
    (new amphux\controleurs\ControleurSoirees())->afficher();
})->name("soirees");

$app->get('/soiree/', function(){
    (new amphux\controleurs\ControleurSoiree())->afficher();
})->name("soiree");

$app->notFound(function (){
    (new amphux\controleurs\Error404Controller())->affichageErreur();
});

$app->get('/urgences', function() {
    print ((new amphux\vues\VueNavigation())->render(\amphux\vues\VueNavigation::AFF_URGENCES));
})->name("urgences") ;


$app->get('/sam', function(){
    print ((new amphux\vues\VueNavigation())->render(\amphux\vues\VueNavigation::AFF_SAM));
})->name("sam");


$app->get('/signaler', function() {
    print ((new amphux\controleurs\ControleurReport())->create());
})->name('signaler') ;

$app->post('/signaler_traitement', function(){
    (new \amphux\controleurs\ControleurReport())->traiter();
})->name('signaler_traitement');

$app->get('/signalements', function() {
    print ((new amphux\vues\VueReport())->show()) ;
})->name('signalements') ;
$app->run();
