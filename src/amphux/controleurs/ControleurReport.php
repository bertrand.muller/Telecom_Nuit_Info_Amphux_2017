<?php
/**
 * Created by PhpStorm.
 * User: brodi
 * Date: 08/12/2017
 * Time: 00:27
 */

namespace amphux\controleurs;


use amphux\models\Report;
use amphux\vues\VueNavigation;
use amphux\models\User ;

class ControleurReport
{
    public function create(){
        //if(ControleurUser::isConnected()){
            $vue = new VueNavigation() ;
            print $vue->render(VueNavigation::AFF_CREATE_REPORT) ;
       // }
    }

    public function traiter(){
        $report = new Report();
        $report->description = $_POST['description'];
        $report->type = $_POST['types'];
        $report->place = $_POST['lat'].";".$_POST['lng'];
        $d = new \DateTime() ;
        $d->setTime($_POST['hours'], $_POST['minutes']) ;
        $report->report_at = $d ;
        $report->plus = 0;
        $report->minus = 0;
        $report->user_id = User::getByEmail($_SESSION['mail'])->user_id;
        $report->save();
        $vue = new VueNavigation() ;
        print $vue->render(VueNavigation::AFF_CREATE_REPORT) ;

    }
}