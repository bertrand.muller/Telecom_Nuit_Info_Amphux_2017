<?php
/**
 * Created by PhpStorm.
 * User: bertr
 * Date: 07/12/2017
 * Time: 20:57
 */

namespace amphux\controleurs;


use amphux\vues\VueSoirees;

class ControleurSoirees {

    public function afficher() {
        $vue = new VueSoirees();
        $vue->render();
    }

}