<?php

namespace amphux\controleurs;
use amphux\vues\Error404View;

class Error404Controller {

	public function __construct() {}

	public function affichageErreur() {
		$er = new Error404View();
		echo $er->render();
	}

}