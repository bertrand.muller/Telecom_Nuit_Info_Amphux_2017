<?php

namespace amphux\controleurs;

use \amphux\vues\VueNavigation;
use \amphux\models\User;
use amphux\vues\VuePageHTML;

class ControleurUser
{

    public function index(){
        $vue = new VueNavigation();
        print $vue->render(VueNavigation::AFF_INDEX);
    }

    public function deconnexion(){
       
        if(isset($_SESSION['mail'])){
            unset($_SESSION['mail']);
        }
        if(isset($_SESSION['admin'])){
            unset($_SESSION['admin']);
        }
        $app =  \Slim\Slim::getInstance();
        $app->redirect($app->urlFor("accueil"));
    }

    public function connexion(){
        $app =  \Slim\Slim::getInstance();
        $requete = $app->request;
        $password = filter_var($requete->post("mdpCon"),FILTER_SANITIZE_SPECIAL_CHARS);
        $mail = filter_var($requete->post("mailCon"),FILTER_SANITIZE_SPECIAL_CHARS);
        $user = User::getByEmail($mail);
        if($user != null) {
            if (password_verify($password, $user->password)) {
                $_SESSION['mail'] = $user->mail;
                // CONNEXION OK
                array_push($_SESSION['msg'],VuePageHTML::MSG_CONNEXION_OK);
                $app->redirect($app->urlFor("accueil"));
            } else {
                // ERREUR MDP FAUX
                array_push($_SESSION['msg'],VuePageHTML::MSG_CONNEXION_ERROR);
                $app->redirect($app->urlFor("accueil"));
            }
        }else{
           // ERREUR LOGIN INCONNU
            array_push($_SESSION['msg'],VuePageHTML::MSG_CONNEXION_ERROR);
            $app->redirect($app->urlFor("accueil"));
        }
    }

    public static function isConnected(){
        $coo = false ;
        if($_SESSION['msg']==MSG_CONNEXION_OK)
            $coo = true ;
        return $coo ;
    }

    public function inscription(){
        $app =  \Slim\Slim::getInstance();
        $requete = $app->request;
        $password = $requete->post("mdp1");
        $verification = $requete->post("mdp2");
        $mail = filter_var($requete->post("mail"),FILTER_SANITIZE_EMAIL);
        $pseudo = $requete->post("nom");

        $error = false;

        if ($password != '' && $verification != ''){
            if($password === $verification){
                if($password == filter_var($password, FILTER_SANITIZE_STRING)){
                    $password = filter_var($password, FILTER_SANITIZE_STRING);
                }else{
                    $error = true;
                    $_SESSION['msg'][] = VuePageHTML::MSG_INSCRIPTION_ERROR_MDP_INVALIDE;
                }
            }else{
                $error = true;
                $_SESSION['msg'][] = VuePageHTML::MSG_INSCRIPTION_ERROR_MDP_NON_INDENTIQUES;
            }
        }else{
            $error = true;
            $_SESSION['msg'][] = VuePageHTML::MSG_INSCRIPTION_ERROR_MDP_MANQUANT;
        }

        if ($pseudo != ''){
            if($pseudo == filter_var($pseudo, FILTER_SANITIZE_STRING)){
                $pseudo = filter_var($pseudo, FILTER_SANITIZE_STRING);
            }else{
                $error = true;
                $_SESSION['msg'][] = VuePageHTML::MSG_INSCRIPTION_ERROR_NOM_INVALIDE;
            }
        }else{
            $error = true;
            $_SESSION['msg'][] = VuePageHTML::MSG_INSCRIPTION_ERROR_NOM_MANQUANT;
        }

        if ($mail != ''){
            if(filter_var($mail, FILTER_VALIDATE_EMAIL)){
                $user = User::getByEmail($mail);
                if($user != null){
                    $error = true;
                    $_SESSION['msg'][] = VuePageHTML::MSG_INSCRIPTION_ERROR_EMAIL_DEJA_UTILISE;
                }else {
                    $mail = filter_var($mail, FILTER_SANITIZE_EMAIL);
                }
            }else{
                $error = true;
                $_SESSION['msg'][] = VuePageHTML::MSG_INSCRIPTION_ERROR_EMAIL_INVALIDE;
            }
        }else{
            $error = true;
            $_SESSION['msg'][] = VuePageHTML::MSG_INSCRIPTION_ERROR_MDP_MANQUANT;
        }

        if (!$error){
            $password = password_hash($password, PASSWORD_DEFAULT, Array('cost' => 12));
            $u = new User();
            $u->mail = $mail;
            $u->password = $password;
           // $u->nom = $pseudo;
            $u->save();

            $_SESSION['mail'] = $mail;

            $_SESSION['msg'][] = VuePageHTML::MSG_INSCRIPTION_OK;
            $app->redirect($app->urlFor("accueil"));
        }else{
            $app->redirect($app->urlFor("accueil"));
        }
    }
}