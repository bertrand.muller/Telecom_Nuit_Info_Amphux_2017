<?php

namespace amphux\models;

use \amphux\models\Appartient;

class Report extends \Illuminate\Database\Eloquent\Model {
    protected $table = "report";
    protected $primaryKey = "id_report";
    public $timestamps = false;

	public function user()
    {
        return $this->belongsTo('amphux\models\User', 'user_id');
    }
	
  
}

