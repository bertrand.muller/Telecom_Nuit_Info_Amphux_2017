<?php

namespace amphux\models;

use \amphux\models\Appartient;

class User extends \Illuminate\Database\Eloquent\Model {
    protected $table = "user";
    protected $primaryKey = "user_id";
    public $timestamps = false;

    public static function getByEmail($mail){
        $email = filter_var($mail,FILTER_SANITIZE_EMAIL);
        return User::where('mail','=',$mail)->first();
    }
	
	public function report(){
		return $this->hasMany('amphux\models\Report','id_report');
	}
	
	public function path(){
		return $this->hasMany('amphux\models\Path','id_path');
	}
}


