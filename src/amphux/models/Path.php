<?php

namespace amphux\models;

use \amphux\models\Appartient;

class Path extends \Illuminate\Database\Eloquent\Model {
    protected $table = "path";
    protected $primaryKey = "id_path";
    public $timestamps = false;

   public function user(){
		return $this->belongsTo('amphux\models\User','user_id');
   }
}

