<?php
/**
 * Created by PhpStorm.
 * User: ophelien
 * Date: 09/02/17
 * Time: 15:21
 */

namespace amphux\vues;


class VuePageHTML
{
    const TYPE_MESSAGE_INFO = "blue";
    const TYPE_MESSAGE_WARNING = "orange";
    const TYPE_MESSAGE_ERROR = "red";

    const MSG_CONNEXION_OK = "MSG_CONNEXION_OK";
    const MSG_CONNEXION_ERROR = "MSG_CONNEXION_ERROR";
    const MSG_INSCRIPTION_OK = "MSG_INSCRIPTION_OK";
    const MSG_INSCRIPTION_ERROR_NOM_MANQUANT = "MSG_INSCRIPTION_ERROR_NOM_MANQUANT";
    const MSG_INSCRIPTION_ERROR_NOM_INVALIDE = "MSG_INSCRIPTION_ERROR_NOM_INVALIDE";
    const MSG_INSCRIPTION_ERROR_EMAIL_MANQUANT = "MSG_INSCRIPTION_ERROR_EMAIL_MANQUANT";
    const MSG_INSCRIPTION_ERROR_EMAIL_INVALIDE = "MSG_INSCRIPTION_ERROR_EMAIL_INVALIDE";
    const MSG_INSCRIPTION_ERROR_EMAIL_DEJA_UTILISE = "MSG_INSCRIPTION_ERROR_EMAIL_DEJA_UTILISE";
    const MSG_INSCRIPTION_ERROR_MDP_MANQUANT = "MSG_INSCRIPTION_ERROR_MDP_MANQUANT";
    const MSG_INSCRIPTION_ERROR_MDP_INVALIDE = "MSG_INSCRIPTION_ERROR_MDP_INVALIDE";
    const MSG_INSCRIPTION_ERROR_MDP_NON_INDENTIQUES = "MSG_INSCRIPTION_ERROR_MDP_NON_INDENTIQUES";

    private static function getMessages(){
        $retour = "";
        //var_dump($_SESSION);
        foreach($_SESSION['msg'] as $msg){
            switch ($msg){
                case VuePageHTML::MSG_CONNEXION_OK:
                    $retour .= VuePageHTML::afficherMessage("Vous êtes connecté",VuePageHTML::TYPE_MESSAGE_INFO);
                    break;
                case VuePageHTML::MSG_CONNEXION_ERROR:
                    $retour .= VuePageHTML::afficherMessage("Votre email ou mot de passe est faux",VuePageHTML::TYPE_MESSAGE_ERROR);
                    break;
                case VuePageHTML::MSG_INSCRIPTION_OK:
                    $retour .= VuePageHTML::afficherMessage("Votre inscription a été validée",VuePageHTML::TYPE_MESSAGE_INFO);
                    break;
                case VuePageHTML::MSG_INSCRIPTION_ERROR_NOM_MANQUANT:
                    $retour .= VuePageHTML::afficherMessage("Vous devez saisir votre nom",VuePageHTML::TYPE_MESSAGE_ERROR);
                    break;
                case VuePageHTML::MSG_INSCRIPTION_ERROR_NOM_INVALIDE:
                    $retour .= VuePageHTML::afficherMessage("Vous devez saisir un nom valide",VuePageHTML::TYPE_MESSAGE_ERROR);
                    break;
                case VuePageHTML::MSG_INSCRIPTION_ERROR_EMAIL_MANQUANT:
                    $retour .= VuePageHTML::afficherMessage("Vous devez saisir une adresse email",VuePageHTML::TYPE_MESSAGE_ERROR);
                    break;
                case VuePageHTML::MSG_INSCRIPTION_ERROR_EMAIL_INVALIDE:
                    $retour .= VuePageHTML::afficherMessage("Votre adresse email est invalide",VuePageHTML::TYPE_MESSAGE_ERROR);
                    break;
                case VuePageHTML::MSG_INSCRIPTION_ERROR_EMAIL_DEJA_UTILISE:
                    $retour .= VuePageHTML::afficherMessage("L'email saisi est déjà utilisé",VuePageHTML::TYPE_MESSAGE_ERROR);
                    break;
                case VuePageHTML::MSG_INSCRIPTION_ERROR_MDP_MANQUANT:
                    $retour .= VuePageHTML::afficherMessage("Vous devez saisir un mot de passe",VuePageHTML::TYPE_MESSAGE_ERROR);
                    break;
                case VuePageHTML::MSG_INSCRIPTION_ERROR_MDP_INVALIDE:
                    $retour .= VuePageHTML::afficherMessage("Votre mot de passe est invalide",VuePageHTML::TYPE_MESSAGE_ERROR);
                    break;
                case VuePageHTML::MSG_INSCRIPTION_ERROR_MDP_NON_INDENTIQUES:
                    $retour .= VuePageHTML::afficherMessage("La confirmation de votre mot de passe est faux",VuePageHTML::TYPE_MESSAGE_ERROR);
                    break;
                default:
                    $retour .= VuePageHTML::afficherMessage("Message inconnu",VuePageHTML::TYPE_MESSAGE_ERROR);
            }
        }
        $_SESSION['msg'] = array();
        return $retour;
    }

    private static function afficherMessage($text, $type){
        return <<<END
<script>
  Materialize.toast($('<span>').append($("<span>").text("$text")), 10000, '$type');
</script>
END;
    }

    public static function getHeaders(){
        $app = \Slim\Slim::getInstance();
        $racine = substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME'])-9);
        $messages = VuePageHTML::getMessages();
        return <<<end
<!DOCTYPE html>
<html>
    <head>
        <script src="/Amphux/js/jquery-3.2.1.min.js"></script>
        <link rel="shortcut icon" href="{$racine}img/favicon.ico" type="image/x-icon">
        
         <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		 <link rel="stylesheet" href="{$racine}css/css.css">
         <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
        
        <title>Amphux security</title>
<!-- custom-theme -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Events Planning Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //custom-theme -->

<link href="{$racine}css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="{$racine}css/style.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" href="{$racine}css/flexslider.css" type="text/css" media="screen" property="" />
<!-- js -->
<script type="text/javascript" src="/Amphux/js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<!-- font-awesome-icons -->
<link href="{$racine}css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome-icons -->
<link href="//fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    </head>
<body>
$messages
end;
    }

    public static function getFooter(){
        return <<<end
</body>
</html>
end;
    }

    public static function getMenu(){
        $app = \Slim\Slim::getInstance();
        $racine = $app->request->getResourceUri();
        $r_accueil = $app->urlFor("accueil");
        $r_aff_coloc = "";//$app->urlFor("amphux");
        $r_creer_coloc = "";//$app->urlFor("creeramphux");
        $r_deconnexion = $app->urlFor("deconnexion");
        $r_connexion = $app->urlFor("connexion");
        $r_inscription = $app->urlFor("inscription");
        $r_signaler = $app->urlFor("signaler");

        if(isset($_SESSION['mail'])){
            $connexion = "<a href=\"$r_deconnexion\" class=\"link link--kumya\"><i class=\"fa fa-cog\" aria-hidden=\"true\"></i><span data-letters=\"Deconnexion\">Deconnexion</span></a>";
        }else{
            $connexion = "<a href=\"$r_connexion\" class=\"link link--kumya\"><i class=\"fa fa-cog\" aria-hidden=\"true\"></i><span data-letters=\"Connexion\">Connexion</span></a>";
        }

        if(isset($_SESSION['admin'])){
            $groupe = "<a class=\"waves-effect waves-light btn-large\" href=\"$r_creer_coloc\">Pannel admin</a>";
        }else {
            if (isset($_SESSION['idGroupe'])) {
                $groupe = "<a class=\"waves-effect waves-light btn-large\" href=\"$r_aff_coloc\">Ma coloc'</a>";
            } else {
                $groupe = "<a class=\"waves-effect waves-light btn-large\" href=\"$r_creer_coloc\">Créer colloc'</a>";
            }
        }

        return <<<end
		
<html lang="en">
<body>
<!-- banner -->
<div class="w3_agile_menu">
	<div class="agileits_w3layouts_nav">
		<div id="toggle_m_nav">
			<div id="m_nav_menu" class="m_nav">
				<div class="m_nav_ham w3_agileits_ham" id="m_ham_1"></div>
				<div class="m_nav_ham" id="m_ham_2"></div>
				<div class="m_nav_ham" id="m_ham_3"></div>
			</div>
		</div>
		<div id="m_nav_container" class="m_nav wthree_bg">
			<nav class="menu menu--sebastian">
				<ul id="m_nav_list" class="m_nav menu__list">
					<li class="m_nav_item active" id="m_nav_item_1"> <a href="$r_accueil" class="link link--kumya"><i class="fa fa-home" aria-hidden="true"></i><span data-letters="Home">Home</span></a></li>
					<li class="m_nav_item" id="moble_nav_item_2"> $connexion</li>
					<li class="m_nav_item" id="moble_nav_item_3"> <a href="$r_inscription" class="link link--kumya"><i class="fa fa-info-circle" aria-hidden="true"></i><span data-letters="Inscription">Inscription</span></a></li>
					<li class="m_nav_item" id="moble_nav_item_4"> <a href="$r_signaler" class="link link--kumya"><i class="fa fa-building-o" aria-hidden="true"></i><span data-letters="Signaler">Signaler</span></a></li>
				</ul>
			</nav>
		</div>
	</div>
</div>
	<div class="banner">
		<div class="container">
			<!-- header -->


	</section>
			<!-- flexSlider -->
				<script defer src="/Amphux/js/jquery.flexslider.js"></script>
				<script type="text/javascript">
					$(window).load(function(){
					  $('.flexslider').flexslider({
						animation: "slide",
						start: function(slider){
						  $('body').removeClass('loading');
						}
					  });
					});
				</script>
			<!-- //flexSlider -->
			</div>
		</div>
	</div>
<!-- //banner -->

<!-- start-smoth-scrolling -->
<script type="text/javascript" src="/Amphux/js/move-top.js"></script>
<script type="text/javascript" src="/Amphux/js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<!-- menu -->
	<script type="text/javascript" src="/Amphux/js/main.js"></script>
<!-- //menu -->
<!-- for bootstrap working -->
	<script src="/Amphux/js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear'
				};
			*/

			$().UItoTop({ easingType: 'easeOutQuart' });

			});
	</script>
<!-- //here ends scrolling icon -->
</body>
</html>


	
end;

    }
}