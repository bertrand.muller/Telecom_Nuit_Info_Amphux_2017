<?php

namespace amphux\vues;

use \amphux\vues\VuePageHTML;

class VueNavigation
{
    const AFF_INDEX = 1;
    const AFF_CONNEXION = 2;
    const AFF_INSCRIPTION = 3;
    const AFF_URGENCES = 4 ;
    const AFF_SAM = 5;
    const AFF_CREATE_REPORT = 6 ;
    const AFF_ALL_REPORT = 7 ;

    private $objet;

    public function __construct($array = null)
    {
        $this->objet = $array;
    }

    public function render($selecteur)
    {
        switch ($selecteur) {
            case VueNavigation::AFF_INDEX :
                $content = $this->index();
                break;
            case VueNavigation::AFF_CONNEXION :
                $content = $this->connexion();
                break;
            case VueNavigation::AFF_INSCRIPTION :
                $content = $this->inscription();
                break;
            case VueNavigation::AFF_URGENCES :
                $content = PageUrgences::render() ;
                break ;
            case VueNavigation::AFF_SAM :
                $content = PageSam::render() ;
                break ;
            case VueNavigation::AFF_CREATE_REPORT :
                $content = VueReport::create() ;
                break ;
            case VueNavigation::AFF_ALL_REPORT :
                $content = VueReport::show() ;
                break ;
        }
        if(isset($_SESSION["email"])){

        }
        return VuePageHTML::getHeaders().VuePageHTML::getMenu().$content.VuePageHTML::getFooter();
    }

    private function index(){
        $app = \Slim\Slim::getInstance();
        $r_urgences = $app->urlFor("urgences");
        $r_sam = $app->urlFor("sam");
        $racine = substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME'])-9);
        $r_logements = "";


        $mess = "";
        $error = $this->objet;
        if($error != null){
            foreach($error as $value){
                $mess .='<p class="red-text">' . $value . '<br></p>';
            }
        }
        return <<<end
<div class="test" >
$mess
<a href="$r_urgences" id="urgences" >Urgences</a>
<a href="$r_sam" class = "nrml" id="urgences" >Bla bla sam!</a>
<a href="" class = "nrml" id="urgences" >Rapport</a>
<a href="" class = "nrml" id="urgences" >Informations</a>
<a href="" class = "nrml" id="urgences" >Prevention</a>

</div>
end;
    }

    private function connexion(){
        $app = \Slim\Slim::getInstance();
        $r_connexion = $app->urlFor("connexion");
        $r_inscription = $app->urlFor("inscription");
        return <<<end
<br/>
<br/>
<div class="row">
    <form id="formulaire_connexion" class="for" method="POST" action="$r_connexion">
        <div class="row">
            <div class="input-field">
                <input placeholder="Dupont@gmail.com" type="text" name="mailCon" id="connexion_mail" required>
                <label class="black-text">Adresse mail</label>
            </div>
            <br/>
            <div class="input-field">
                <input placeholder="*********"  type="password" name="mdpCon" id="connexion_mdp" required>
                <label class="black-text">Mot de passe</label>
            </div>
        </div>
        <br/><br/>
        <a id="boutton_connexion" >Se connecter</a>
    </form>
    <br/><br/>
    <a href="$r_inscription" id="boutton_pasEncoreInscrit">Pas encore inscrit ?</a>
</div>
<script>
     $("#boutton_connexion").click(function() {
        if($("#connexion_mail").val() == ""){
            alert("Vous devez saisir votre adresse email");
        }else if($("#connexion_mdp").val() == ""){
            alert("Vous devez saisir votre mot de passe");
        }else{
            $("#formulaire_connexion").submit();
        }
    });
</script>
end;

    }

    private function inscription(){
        $app = \Slim\Slim::getInstance();
        $r_inscription = $app->urlFor("inscription");

        return <<<end
        <br/>
        <br/>
<form id="formulaire_inscription" class="for" method="POST" action="$r_inscription">
        <div class="row">
            <div class="input-field">
                <input placeholder="Dupont"  type="text" name="nom" id="inscription_nom" required>
                <label class="black-text">Nom d'utilisateur</label>
            </div>
            <br/>
            <div class="input-field">
                <input placeholder="Dupont@gmail.com" type="text" name="mail" id="inscription_mail" required>
                <label class="black-text">Adresse mail</label>
            </div>
            <br/>
            <div class="input-field">
                <input placeholder="*********"  type="password" name="mdp1" id="inscription_mdp1" required>
                <label class="black-text">Mot de passe</label>
            </div>
            <br/>
            <div class="input-field">
                <input placeholder="*********"  type="password" name="mdp2" id="inscription_mdp2" required>
                <label class="black-text">Confirmation du mot de passe</label>
            </div>
        </div>
        <br/><br/>
        <a id="boutton_inscription">S'inscrire gratuitement</a>
    </form>
<script>
    $("#boutton_inscription").click(function() {
        if($("#inscription_mail").val() == ""){
            alert("Vous devez saisir une adresse email");
        }else if($("#inscription_nom").val() == ""){
            alert("Vous devez saisir un nom");
        }else if($("#inscription_mdp1").val() == ""){
            alert("Vous devez saisir un mot de passe");
        }else if($("#inscription_mdp1").val() != $("#inscription_mdp2").val()){
            alert("Vos mots de passes sont incorrects");
        }else{
            $("#formulaire_inscription").submit();
        }
    });
</script>
end;

    }

}
