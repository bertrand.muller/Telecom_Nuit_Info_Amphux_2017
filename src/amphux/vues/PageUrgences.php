<?php
/**
 * Created by PhpStorm.
 * User: Cath
 * Date: 07/12/2017
 * Time: 19:31
 */
namespace  amphux\vues ;
class PageUrgences
{
    public static function render()
    {
        $html = <<<HTML
<div style="margin-top: 2%">
    <h1> Numéros d'urgence</h1>
    <p> <b> SAMU </b> : 15</p>
    <p> <b> Pompiers </b> : 18</p>
    <p> <b> Police </b> : 17</p>
    <p> <b> Urgences européennes </b> : 112</p>
</div>
<div>
    <h1 style="margin-top: 2%"> En cas d'accident </h1>
<!-- A faire en cas d'urgence -->
<details>
    <summary> <h3>Protéger</h3> </summary> 
    <div style="width: 75%; margin: auto;"> 
        <ul style = "text-align: justify; text-justify: auto;"> 
        <li> Si je suis témoin : 
            <ul>
                <li> Se garer 100 mètres après l'accident.</li>
                <li> Mettre les passagers à l'abri (sur le bas côté). </li>
            </ul>
        </li>
        <li> Allumer ses feux de détresses. </li>
        <li> Placer des triangles de signalisation à au moins 200 mètres du lieu de l'accident. Ils doivent être visibles par les automobilistes afin d'éviter un sur-accident. </li>
        <li> Porter un gilet haute visibilité si vous circulez à pied aux environs de l'accident.</li>
        <li> Eviter les attroupements. Trop de personnes risquent de gêner les victimes et les secours. </li>
        <li> Dans la mesure du possible, couper le contact et serrer le frein à main du ou des véhicule(s) accidenté(s). </li>
        <li> Sortir les extincteurs des véhicules afin de pouvoir réagir en cas d'incendie. </li>
        <li> Se placer derrière la glissière de sécurité car il est dangereux de circuler aux abords de la route, même si les véhicules sont alertés de l'accident. </li>
        <li> Eviter de modifier l'état des lieux afin de permettre aux secours de mieux évaluer les circonstances de l'accident. </li>
        <li> Ne pas fumer afin d'éviter de provoquer un incendie. </li>
     </ul>  
     </div>
</details>
<details>
    <summary> <h3>Alerter</h3> </summary>  
    <div> 
         <h5> Une fois les lieux protégés, appeler les secours </h5>
        <ul>
        <li> Si vous êtes sur une autoroute privilégiez les bornes d'appel. Elles permettent aux secours de connaître votre position exacte.</li>
        <li> Durant l'appel, suivre cette procédure :
            <ul>
                <li> Donner son numéro de téléphone. Ainsi, les secours pourront vous recontacter en cas de coupure ou s'ils ont besoin de précisions. </li>
                <li> Donner la localisation précise de l'accident. </li>
                <li> Décrire les dégâts matériels : nombre et état des véhicules impliqués. </li>
                <li> Détailler le nombre de blessés ainsi que leur état.</li>
                <li> Ne pas raccrocher en premier. </li>
            </ul>
        </li>
    </ul>
    </div>
</details>
<details>
    <summary> <h3>Secourir</h3> </summary>
    <div> 
        <h5> En attendant l'arrivée des secours prenez soin des victimes.</h5>
        <ul>
        <li> Couvrir la victime. Elle est sûrement en état de choc et risque de prendre froid.</li>
        <li> Parler à la victime afin de la maintenir éveillée. Elle vous entendra même si elle est inconsciente et cela la rassurera.</li>
        <li> Dans la mesure du possible, éviter de déplacer la victime. Cela risquerait d'aggraver une blessure existante. </li>
        <li> Ne pas nourrir la victime. En effet, certaines opérations se pratiquent à jeun.</li>
        <li> Ne pas enlever les casques des motards car cela pourrait aggraver leur état de santé. </li>
        <li> Gestes de premier secours :
            <details>
                <summary> En cas d'hémorragie... </summary>
                <div>
                    <ul>
                    <li> Allonger la victime. </li>
                    <li> Comprimer la plaie. Ne pas le faire directement : utiliser un linge propre. </li>
                    <li> Si le saignement est très abondant mettre en place un garot. </li>
                    </ul> 
                </div>
            </details>
            <details>
                <summary> Si la victime ne respire plus...</summary>
                <div>
                    <ul>
                    <li> Le massage cardiaque 
                        <ul>
                        <li> Allonger la victime sur le dos.</li>
                        <li> S'agenouiller à côté d'elle et placer ses deux mains jointes au milieu de sa poitrine. </li>
                        <li> Bras tendus, comprimer le sternum de la victime. Le laisser ensuite revenir à sa position normale avant d'effectuer une nouvelle compression. </li>
                        <li> Idéalement, il faut aller à un rythme de 100 battements par minutes, comme dans la musique <a href="https://www.youtube.com/watch?v=ViwtNLUqkMY">Crazy in love </a> de Beyoncé</li>
                        </ul>
                    </li>
                    <li> Le bouche à bouche
                        <ul>
                        <li> Basuler la tête de la victime vers l'arrière et soulever son menton.</li>
                        <li> Maintenir sa tête dans cette position (en plaçant sa main sur son front) et pincer ses narines. </li>
                        <li> Couvrir la bouche de la victime par la vôtre et expirer lentement et régulièrement de l'air dans sa bouche.</li>
                        <li> Chaque insuflation doit durer environ une seconde. </l>
                        </ul>
                    </li>
                    </ul>
                </div>
            </details>
            <details>
                <summary> Lorsque la victime est inconsciente...</summary>
                <div>
                    <h5> Placer la victime en position latérale de séurité</h5>
                    <ul>
                        <li> Placer le bras de la victime le plus proche de vous en angle droit par rapport à son corps. Tourner sa paume vers le haut.</li>
                        <li> Saisir l'autre bras de la victime et placer le dos de sa main contre son oreille, du côté du bras précédemment bougé. Garder votre main colée à la sienne.</li>
                        <li> Avec l'autre main, prendre la jambe la plus éloignée de vous et la relever, tout en gardant le pied au sol. </li>
                        <li> Faire rouler la victime en tirant sur sa jambe, jusqu'à ce que le genou de la victime touche le sol. </li>
                        <li> Lâcher délicatement la victime. </li>
                        <li> Ouvrir la bouche de la victime sans bouger sa tête afin de permettre l'écoulement des liquides.</li>
                    </ul>
                </div>
            </details>   
        </li>
    </ul>
    </div>
</details>
HTML;

        return $html;
    }

}