<?php
/**
 * Created by PhpStorm.
 * User: bertr
 * Date: 07/12/2017
 * Time: 21:10
 */

namespace amphux\vues;


class VueSoirees
{

    public function __construct() {
       //$this->json = $j;
    }

    public function render() {
        $racine = substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME'])-9);
        echo VuePageHTML::getHeaders();
        include('html/soirees.html');
    }

}