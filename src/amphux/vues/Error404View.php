<?php

namespace amphux\vues;

class Error404View {

	public function __construct() {}

	public function render() {
        $app = \Slim\Slim::getInstance();
        $racine = substr($_SERVER['SCRIPT_NAME'],0,strlen($_SERVER['SCRIPT_NAME'])-9);
        $html = <<<end
<!DOCTYPE html>
<html>
<head>
        <script src="{$racine}js/jquery-3.2.1.min.js"></script>
        <link rel="shortcut icon" href="{$racine}img/favicon.ico" type="image/x-icon">
        <link type="text/css" rel="stylesheet" href="{$racine}css/materialize.min.css"  media="screen,projection"/>
         <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
		<link rel="stylesheet" href="{$racine}css/css.css">
		<link rel="stylesheet" href="{$racine}css/404css.css">
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
         <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>           
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script> 
    </head>
<body> 
<div>
<p id="explications" style="color: white; font-size: 17px;">
    Vous venez de tomber sur une page d'erreur. Ceci peut être dû à une mauvaise saisie d'URL, ou dû à une URL plus valide.<br>
    Veuillez cliquer sur le bouton ci-dessous pour être redirigé vers le site web Amphux.
</p>
<input type="submit" value="Redirection vers le site web Amphux" id="valider" />
</div>
<div class = "test" id="jeu" style="display: none;margin-bottom: 0;">
<canvas id='miniJeu' width="1050" height="630">

</canvas>
</div>
<script type="text/javascript" src="{$racine}js/main404.js"></script>
<script type="text/javascript">
    $("#valider").on('click', function(e){
       $("#valider").fadeOut();
       $("#explications").fadeOut();
       $("#jeu").fadeIn();
       
    });    
</script> </body>
</html>
end;
		return $html;
    }
}