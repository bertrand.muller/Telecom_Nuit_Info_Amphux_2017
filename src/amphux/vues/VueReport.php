<?php
/**
 * Created by PhpStorm.
 * User: brodi
 * Date: 07/12/2017
 * Time: 23:38
 */

namespace amphux\vues;
use Slim\Slim;
use amphux\models\Report ;

class VueReport
{
    public static function create()
    {
        $app = Slim::getInstance();
        $reportH = 'reportHour';
        $html = <<<HTML
        <br/>
        <br/>
        <form id="formReport" name="createReport" action="{$app->urlFor('signaler_traitement')}" method="post">
            <p>Type d'Incident<br/>
                <select name="types" required>
                    <option value="accident">Accident</option>
                    <option value="ralentissements">Ralentissements</option>
                    <option value="travaux">Travaux</option>
                    <option value="verglas">Verglas</option>
                    <option value="route enneigée">Route enneigée</option>
                    <option value="accès bloqué">Accès bloqué</option>
                    <option value="panne">Panne</option>
                </select>
            </p>
            <br/>
            <p>Description de la Situation
            <br/>
            <input type="text" name="description" required /></p>
            <br/>
            <p>Localisation </p>
            <iframe src="/Amphux/src/amphux/models/Map.html" height="500px" width="50%"> 
            <!-- Insérer API GM -->
            </iframe>
            <script>
            window.onfocus = function() {
                var pr = $.ajax ({
                type: "POST",
                url: "../js/ajax/getCoordonneesMap.php",
                dataType : "json"
            });
            pr.done(function (data, status, jqXHR) {
              var x = data[0];
              var y = data[1];
              var ch1 =document.getElementById("lat");
              var ch2 =document.getElementById("lng");
              ch1.value=x;
              ch2.value=y;
              
              //console.log(x+"-"+y);
            });
            pr.fail(function (error, status, text) {
                alert("ERREUR : " + error.status);
                console.log(error);
            });
            }
            </script>
            <input type="hidden" id="lat" name="lat"/>
            <input type="hidden" id="lng" name="lng"/>
            <br/>
            <br/>
            <p>Vu à<br/>
                <select name="hours" required>
HTML;
                $html.= self::reportHour(23) ;
                $html.= <<<HTML
                </select>
                h
                <select name="minutes" required>
HTML;
                  $html.= self::reportHour(59);
                $html.= <<<HTML
            </select>
            </p>
            <br/>
            <p><input type="submit" id="validerReport" value="Valider"></p>
        </form>
        <br/>
HTML;
                return $html ;
    }

    public static function reportHour($entierArrivee)
    {
        $htmlcode = "";
        for ($i = 0; $i <= $entierArrivee; $i++) {
            $htmlcode .= "<option value=$i>$i</option>";
        }
        return $htmlcode;
    }

    public static function show() {
        $report = Report::get()  ;
        $html = "" ;
        $nb = 0 ;
       foreach($report as $r) {
           $ad = 'ad'.$nb ;
           switch($r->type) {
               case 'accident' :
               case 'accès bloqué' :
               $html.= "<div><p> Un ". $r->type . " a été repéré à cette adresse : <span id={$ad}></span></p>" ;
               break ;
               case 'ralentissements' :
               case 'travaux' :
               $html.= "<div><p> Des ". $r->type . " ont été repérés à cette adresse : <span id={$ad}></span></p>" ;
               break ;
               case 'verglas' :
                   $html.= "<div><p> Une zone verglacée a été repérée à cette adresse : <span id={$ad}></span></p>" ;
                   break ;
               case "route enneigée" :
               case "panne" :
               $html.= "<div><p> Une ". $r->type . " a été repérée à cette adresse : <span id={$ad}></span></p>" ;
               break ;
           }

            $html.= "<p>Description : ". $r->description . "</p></div>" ;
            $nb++ ;
       }


        $html.= <<<HTML
        <div id="map"></div>
        <script>
        function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: {lat: 40.731, lng: -73.997}
  });
  var geocoder = new google.maps.Geocoder;
  var infowindow = new google.maps.InfoWindow;
  var nb = 0 ;
  {$report}.forEach(function(e) {
      
  var input = e.place ;
  var latlngStr = input.split(';', 2);
  var latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
  geocoder.geocode({'location': latlng}, function(results, status) {
    if (status === 'OK') {
      if (results[1]) {
        document.getElementById('ad'+nb).prepend(results[1].formatted_address) ;
    } 
    else {
          console.log('Erreur') ;
    }
    }
    else {
      window.alert('Geocoder failed due to: ' + status);
    }
    nb++ ;
  });
  })
  
  }
  </script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjPhR1VGL-LOjTwBBnbUWllBSUwfmWePU&callback=initMap"></script>
HTML;
        return $html ;
    }

}