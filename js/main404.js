var y = 35;
var x = 35;
var xM = [];
var yM = [];
var dMM = [];
var M = [];
xM[1] = 105;
yM[1] = 105;
dMM[1] = 1;
xM[2] = 385;
yM[2] = 385;
dMM[2] = 3;
xM[3] = 455;
yM[3] = 385;
dMM[3] = 2;
var sens = 1;
var taille = 35;
var c = document.getElementById("miniJeu");
var ctx = c.getContext("2d");
var Mur = new Image();
Mur.src ='./sprite404/Mur.jpg';
var trottoir = new Image();
trottoir.src ='./sprite404/trottoir.jpg';
var vide = new Image();
vide.src ='./sprite404/Vide.jpg';
var cle = new Image();
cle.src = './sprite404/cle.gif';
M[1] = new Image();
M[1].src ='./sprite404/M1.gif';
M[2] = new Image();
M[2].src ='./sprite404/M2.gif';
M[3] = new Image();
M[3].src ='./sprite404/M3.png';
var echelle = new Image();
echelle.src = './sprite404/echelle.gif';
var lab;

affichageVoiture();


$(document).keydown(function(e){
    switch(e.keyCode){
        //gauche
        case 37 :
            if (murOuNon(x-taille,y,false)==0) {
                x = x - taille;
                suppressionCle(x,y);
                sens=3;
                clear();
            }
            break;
        //haut
        case 38:
            if (murOuNon(x,y-taille,false)==0) {
                y = y - taille;
                suppressionCle(x,y);
                sens=1;
                clear();
            }
            break;
        //droite
        case 39:
            if (murOuNon(x+taille,y,false)==0) {
                x = x + taille;
                suppressionCle(x,y);
                sens=4;
                clear();
            }
            break;
        //bas
        case 40:
            if(murOuNon(x,y+taille,false)==0) {
                y = y + taille;
                suppressionCle(x,y);
                sens=2;
                clear();
            }
            break;
    }
    for(i=0; i<xM.length;i++){
        if(x==xM[i] && y==yM[i]){
            window.setTimer(document.location.reload(),300);
        }
    }
});

ctx.closePath();
ctx.stroke();

function clear() {
    ctx.clearRect(0,0,c.width, c.height);
    affichageVoiture();
    afficherMur();
    affichageFantomeConM(1);
    affichageFantomeConM(2);
    affichageFantomeConM(3);
}

function affichageVoiture() {
    var voiture = new Image();
    if(sens==1) {
        voiture.src ='./sprite404/voitureN.jpg';
    }
    if(sens==2) {
        voiture.src ='./sprite404/voitureS.jpg';
    }
    if(sens==3) {
        voiture.src ='./sprite404/voitureO.jpg';
    }
    if(sens==4) {
        voiture.src ='./sprite404/voitureE.jpg';
    }
    
    voiture.onload = function() {
        ctx.drawImage(voiture, x, y,taille, taille);
    };
}

function getLevel(level) {
    var pr = $.ajax ({
        type: "POST",
        url: "./js/ajax/generationLabyrinthe.php",
        dataType: "json",
        data: {"level": level}
    });
    pr.done(function (data, status, jqXHR) {
        if(data.length == 0) {
            alert("ERREUR : le labyrinthe ne se charge pas");
        }
        else {
            genererLabyrinthe(data);
        }
    });
    pr.fail(function (error, status, text) {
        alert("ERREUR : " + error.status);
        console.log(error);
    });
}

function genererLabyrinthe(labyrinthe) {
    lab=labyrinthe;
    afficherMur();
}

function afficherMur(){
    var i = 0;
    var j = 0;
    var comp = 0;
    lab.forEach(function (ligne) {
        j = 0;
        ligne.forEach(function (colonne) {
            if(colonne == 1) {
                ctx.drawImage(trottoir, i*taille, j*taille ,taille, taille);
            }
            if(colonne == 2) {
                ctx.drawImage(Mur, i*taille, j*taille,taille, taille);
            }
            if(colonne == 0 || colonne == 4 || colonne == 3) {
                ctx.drawImage(vide, i * taille, j * taille, taille, taille);
            }
            if (colonne == 4 && (i != 1 || j != 1)) {
                comp++;
                ctx.drawImage(cle,i * taille, j * taille, (taille-5), (taille-5))
            }
            if(colonne == 3) {
                ctx.drawImage(echelle, i*taille, j*taille,taille, taille);
            }
            j++;
        });
        i++;
    });
    //console.log(comp);
    if(comp!=0&&x==0&&y==35) {
        document.location.reload();
    }
    if(comp==0&&x==0&&y==35) {
        document.location.href = "./";
    }    
}

getLevel(2);

function murOuNon(a,b,monstre) {
    var tab = lab[a/taille][b/taille];
    var res;
    if(monstre && tab == 3){
        res = 1;
    }else{
        if (tab == 1 || tab == 2){
            res = 1;
        }
        else{
            res = 0;
        }
    }
    return res;
}

function suppressionCle(a,b) {
    var res = lab[a/taille][b/taille];
    if (res == 4){
        lab[a/taille][b/taille] = 0;
    }

}

function affichageFantomeConM(num) {
    ctx.drawImage(M[num], xM[num], yM[num], taille, taille);
}

function fantomeConM(num) {
    var prec = dMM[num];

    //dMM[num] = Math.floor(Math.random()*4);
    var facteurX = 0;
    var facteurY = 0;
    var possibilite = [];
    switch(dMM[num]){
        case 0: // haut
            if (murOuNon(xM[num],yM[num]-taille,true) == 0){possibilite.push(0)}
            if (murOuNon(xM[num]+taille,yM[num],true) == 0){possibilite.push(2)}
            if (murOuNon(xM[num]-taille,yM[num],true) == 0){possibilite.push(3)}
            break;
        case 1: // bas OK
            if (murOuNon(xM[num],yM[num]+taille,true) == 0){possibilite.push(1)}
            if (murOuNon(xM[num]+taille,yM[num],true) == 0){possibilite.push(2)}
            if (murOuNon(xM[num]-taille,yM[num],true) == 0){possibilite.push(3)}
            break;
        case 2: // droite
            if (murOuNon(xM[num],yM[num]+taille,true) == 0){possibilite.push(1)}
            if (murOuNon(xM[num],yM[num]-taille,true) == 0){possibilite.push(0)}
            if (murOuNon(xM[num]+taille,yM[num],true) == 0){possibilite.push(2)}
            break;
        case 3: // gauche
            if (murOuNon(xM[num],yM[num]+taille,true) == 0){possibilite.push(1)}
            if (murOuNon(xM[num],yM[num]-taille,true) == 0){possibilite.push(0)}
            if (murOuNon(xM[num]-taille,yM[num],true) == 0){possibilite.push(3)}
            break;
    }
    if(possibilite.length == 0){
        console.log("ERREUR");
        switch(dMM[num]){
            case 0: // haut
                dMM[num] = 1;
                break;
            case 1: // bas
                dMM[num] = 0;
                break;
            case 2: // droite
                dMM[num] = 3;
                break;
            case 3: // gauche
                dMM[num] = 2;
                break;
        }
    }else{
        dMM[num] = possibilite[Math.floor(Math.random()*possibilite.length)];
    }
    if(num == 1){
    console.log(possibilite);
    console.log(dMM[num]);
}
    switch(dMM[num]){
        case 0: // haut
            yM[num] = yM[num]-taille;
            break;
        case 1: // bas
            yM[num] = yM[num]+taille;
            break;
        case 2: // droite
            xM[num] = xM[num]+taille;
            break;
        case 3: // gauche
            xM[num] = xM[num]-taille;
            break;
    }
    if(x==xM[num] && y==yM[num]){
        window.setTimer(document.location.reload(),300);
    }
    affichageFantomeConM(num);
}

function fantomeSuiveur() {
    
}

function fantomeIntelligent() {

}

setInterval(function(){
    clear();
    fantomeConM(1);
    fantomeConM(2);
    fantomeConM(3);
    fantomeSuiveur();
    fantomeIntelligent();
    clear();
}, 475);
